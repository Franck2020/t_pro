-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 19 août 2021 à 02:09
-- Version du serveur :  10.4.18-MariaDB-cll-lve
-- Version de PHP : 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `u291364182_find`
--

-- --------------------------------------------------------

--
-- Structure de la table `appartenir`
--

CREATE TABLE `appartenir` (
  `id_app` int(11) NOT NULL,
  `md_id` int(11) DEFAULT NULL,
  `pays_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id_cat` int(11) NOT NULL,
  `type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id_cat`, `type`) VALUES
(1, 'CNI'),
(2, 'PASSPORT'),
(3, 'AUTRE'),
(4, 'ACTE DE NAISSANCE'),
(5, 'PERMIS DE CONDUIRE'),
(6, 'CARTE DE SEJOUR');

-- --------------------------------------------------------

--
-- Structure de la table `envoyer`
--

CREATE TABLE `envoyer` (
  `id_envoi` int(11) NOT NULL,
  `msg_id` int(11) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `envoyer`
--

INSERT INTO `envoyer` (`id_envoi`, `msg_id`, `users_id`) VALUES
(390, 1, 192);

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id_msg` int(11) NOT NULL,
  `texte` text DEFAULT NULL,
  `type` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`id_msg`, `texte`, `type`) VALUES
(1, 'Votre pièce a été retrouvée', 'e'),
(11, 'Montant bien reçu', 'e'),
(12, 'Bb joué ', 'r'),
(13, 'Bjr à tous ', 'r'),
(14, '1500 FCFA vous sera envoyée dans quelque instant', 'e'),
(15, '2500 FCFA vous sera envoyée dans quelque instant', 'e'),
(16, 'Merci', 'r'),
(17, 'Je me porte bien', 'r'),
(18, 'Bonjour je suis en France au 94600', 'r'),
(19, 'Je suis y arrivé .Merci seigneur ', 'r');

-- --------------------------------------------------------

--
-- Structure de la table `mode_paiements`
--

CREATE TABLE `mode_paiements` (
  `id_mp` int(11) NOT NULL,
  `nom_logo` varchar(20) NOT NULL,
  `text_logo` text NOT NULL,
  `image_logo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `id_pays` int(11) NOT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `photo_drapeau` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`id_pays`, `nom`, `photo_drapeau`) VALUES
(1, 'Cameroon', NULL),
(2, 'Gabon', NULL),
(5, 'France', NULL),
(6, 'Belgium', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pieces`
--

CREATE TABLE `pieces` (
  `id_piece` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `datess` varchar(30) NOT NULL,
  `numero_piece` varchar(50) DEFAULT NULL,
  `nompiece` varchar(30) NOT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `type` varchar(2) DEFAULT NULL,
  `pays_provenance` varchar(30) NOT NULL,
  `date_piece` varchar(30) NOT NULL,
  `cat_id` int(4) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pieces`
--

INSERT INTO `pieces` (`id_piece`, `nom`, `prenom`, `datess`, `numero_piece`, `nompiece`, `photo`, `type`, `pays_provenance`, `date_piece`, `cat_id`, `users_id`) VALUES
(1109, 'Tt', 'Tt', '03-03-2003', '33', '', '', 'a', 'Cameroon', '2019-12-15 15:38:25', 1, 190),
(1112, 'Franck', 'Borel', '05-05-2005', '55', '', '', 's', 'Cameroon', '2019-12-15 18:12:43', 1, 190),
(1119, 'tagant', 'borel', '01-05-2015', '112', '', '', 's', 'Cameroon', '2021-08-16 02:13:30', 1, 192),
(1120, 'Montest', 'Premier', '01-01-2021', '123', '', '', 's', 'Cameroon', '2021-08-16 02:14:59', 1, 192),
(1121, 'aa', 'bb', '01-05-1965', '', '', '', 'r', 'Gabon', '2021-08-19 00:38:46', 1, 192),
(1130, 'Aa', 'Bb', '01-05-1965', '1233', '', 'IMG_20210813_184719.txt', 'pa', 'Gabon', '2021-08-19 01:58:12', 1, 193);

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `num_role` int(11) NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`num_role`, `role`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `datess` varchar(50) NOT NULL,
  `sexe` varchar(1) DEFAULT NULL,
  `tel1` varchar(15) DEFAULT NULL,
  `mdp` varchar(200) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `token` varchar(200) NOT NULL,
  `solde` int(11) NOT NULL,
  `remboursement` int(11) NOT NULL,
  `date_enregistrement` varchar(30) NOT NULL,
  `pays_id` int(11) DEFAULT NULL,
  `role_num` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id_users`, `nom`, `prenom`, `datess`, `sexe`, `tel1`, `mdp`, `email`, `token`, `solde`, `remboursement`, `date_enregistrement`, `pays_id`, `role_num`) VALUES
(190, 'Tagant nguechou', 'Franck borel', '16-09-1993', 'h', '693159280', '51d6ebe17873e62fe5ee6496565df14284b15e37', 'falahometest@gmail.com', 'dmB1yuNF5so:APA91bG-449t3ub3yHyi6Gpb8oPZ9NtGrjuUVV0D5C-nRcutWVEE3rZyMwxQsCxg3j9NzqP4u3OvKQJqq2MUxknhJQfmYTEgXHQ1-Mtu7fDEsFYolYe3p3OmpBVsfCjEf--FSGu8YOSl', 0, 0, '2019-12-14 12:03:35', 1, NULL),
(191, 'Youmen ', 'Didier ', '01-01-2000', 'h', '657382362', '8972c2f11aad155a864e7d5f8dcf481c418d217d', 'percevalino@gmail.com', 'e6S151bED0g:APA91bEzBQ5XDPkM6WXbD9v2yxLh8M4TxbPAceJhwZHa9ZtXkJdhv_yfVqQc0mQvz7DQysjfjXVpYZi1Y_cVhvZvZXDWk4kyydvy3ZyyztLZSCiE9H3VFr6vzt0Hd9f_XeXk3_n7jLe-', 0, 0, '2019-12-15 09:30:10', 1, NULL),
(192, 'tagant', 'borel', '10-02-1993', 'h', '1234', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'aa@bb', 'c7uEBxD2Qt61RGJ9foD7vK:APA91bHi09xTTe-UCzfSgE60_2cbI80PGRTP0Yy52X2osrjm7U1ad6FUucOjQnrJOX4AzdCXjdX2rD-h08gwARYXQbihWqDB7nzNsZNd7q95mCkSqwXR5PCyMP0KzeCeH8qKKHj_N5TB', 0, 0, '2021-07-31 13:00:51', 2, 1),
(193, 'Aa', 'Bb', '01-01-1993', 'h', '0000', '39dfa55283318d31afe5a3ff4a0e3253e2045e43', 'aa@bb', '', 0, 0, '2021-07-31 19:51:05', 2, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `appartenir`
--
ALTER TABLE `appartenir`
  ADD PRIMARY KEY (`id_app`),
  ADD KEY `md_id` (`md_id`),
  ADD KEY `pays_id` (`pays_id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id_cat`);

--
-- Index pour la table `envoyer`
--
ALTER TABLE `envoyer`
  ADD PRIMARY KEY (`id_envoi`),
  ADD KEY `msg_id` (`msg_id`),
  ADD KEY `users_id` (`users_id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id_msg`);

--
-- Index pour la table `mode_paiements`
--
ALTER TABLE `mode_paiements`
  ADD PRIMARY KEY (`id_mp`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`id_pays`);

--
-- Index pour la table `pieces`
--
ALTER TABLE `pieces`
  ADD PRIMARY KEY (`id_piece`),
  ADD KEY `cat_id` (`cat_id`),
  ADD KEY `users_id` (`users_id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`num_role`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`),
  ADD KEY `pays_id` (`pays_id`),
  ADD KEY `role_num` (`role_num`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `appartenir`
--
ALTER TABLE `appartenir`
  MODIFY `id_app` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id_cat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `envoyer`
--
ALTER TABLE `envoyer`
  MODIFY `id_envoi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=391;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id_msg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `mode_paiements`
--
ALTER TABLE `mode_paiements`
  MODIFY `id_mp` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `id_pays` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `pieces`
--
ALTER TABLE `pieces`
  MODIFY `id_piece` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1131;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `num_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `appartenir`
--
ALTER TABLE `appartenir`
  ADD CONSTRAINT `appartenir_ibfk_1` FOREIGN KEY (`md_id`) REFERENCES `mode_paiements` (`id_mp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `appartenir_ibfk_2` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id_pays`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `envoyer`
--
ALTER TABLE `envoyer`
  ADD CONSTRAINT `envoyer_ibfk_1` FOREIGN KEY (`msg_id`) REFERENCES `message` (`id_msg`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `envoyer_ibfk_2` FOREIGN KEY (`users_id`) REFERENCES `users` (`id_users`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `pieces`
--
ALTER TABLE `pieces`
  ADD CONSTRAINT `pieces_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id_cat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pieces_ibfk_2` FOREIGN KEY (`users_id`) REFERENCES `users` (`id_users`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id_pays`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`role_num`) REFERENCES `role` (`num_role`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
