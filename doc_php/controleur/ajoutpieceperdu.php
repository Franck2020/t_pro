<?php
header("Access-Control-Allow-Origin: *");
	if(isset($_POST)){
		include_once('../model/bigModelForMe.php');
		include_once('../model/JWT.php');
		$jwt = new JWT();
		$nbre = $jwt->oauth($_POST['token_oauth']);
		if($nbre == 0){
			// Define the Base64 value you need to save as an image
			$nomphoto = $_POST['nomphoto'];
			$b64 = $_POST['pp'];
			$extension_fichier = $_POST['extension_fichier'];
			if($extension_fichier == 'jpg' OR $extension_fichier == 'jpeg'){
				$nomphoto = str_replace('jpg','txt',$nomphoto);
				$nomphoto = str_replace('jpeg','txt',$nomphoto);
			}else{
				$nomphoto = str_replace($extension_fichier,'txt',$nomphoto);
			}
			$monfichier = fopen("../imagesserveur/".$nomphoto,'w+');
			fputs($monfichier,$b64);
			fclose($monfichier);
			
			function trimUltime($chaine){
				$chaine = trim($chaine);
				$chaine = str_replace("###antiSlashe###t", " ", $chaine);
				$chaine = preg_replace('!\s+!', ' ', $chaine);
				return $chaine;
			}
			
			$id =0;
			$dates = date("Y-m-d H:i:s");
			$langue = $_POST['langue'];
			$id_users = $_POST['id_users'];
			$vvb = array();
			$paysprovenance = htmlspecialchars(addslashes(trimUltime($_POST['pvp'])));
			$categoriepiece = htmlspecialchars(addslashes(trimUltime($_POST['cp'])));
			$numeropiece = htmlspecialchars(addslashes(trimUltime($_POST['np'])));
			$nom = htmlspecialchars(addslashes(trimUltime($_POST['nom'])));
			$prenom = htmlspecialchars(addslashes(trimUltime($_POST['prenom'])));
			$nompiece = htmlspecialchars(addslashes(trimUltime($_POST['nompiece'])));
			$datess = htmlspecialchars(addslashes(trimUltime($_POST['jour']))).'-'.htmlspecialchars(addslashes(trimUltime($_POST['mois']))).'-'.htmlspecialchars(addslashes(trimUltime($_POST['annee'])));
			// $verifie = 0;
			// $noms_photos = array();
			// foreach($_FILES as $n){
					////Testons si les fichiers ne sont pas trop gros
					// if ($n['size'] <= 1000000 AND $n['error'] == 0){
						////Testons si l'extension est autorisée
						// $infosfichier =pathinfo($n['name']);
						// $noms_photos[] = $n['name'];
						// $extension_upload = $infosfichier['extension'];
						// $extensions_autorisees = array('jpg', 'JPG','jpeg','JPEG','png','PNG');
					// if (in_array($extension_upload,$extensions_autorisees)){
						////On peut valider le fichier et le stocker définitivement
						// move_uploaded_file($n['tmp_name'], '../imagesserveur/'.basename($n['name']));
						// $verifie += 1;
					// }
				// }
			// }
			// if($verifie == $nbre){
				// $n = $noms_photos[0];
				$tab = $manager->selectionUnique2('categories',array('id_cat'),"type ='$categoriepiece'");
				$tab = $tab[0]->id_cat;
				if(true){
					$envoi = $manager->selectionUnique2('pieces',array('*'),"nom like '%$nom%' AND prenom like '%$prenom%' AND datess='$datess' AND pays_provenance='$paysprovenance' AND cat_id=$tab AND type='s'");
					$envoi6 = $manager->selectionUnique2('pieces',array('*'),"nom like '%$nom%' AND prenom like '%$prenom%' AND datess='$datess' AND pays_provenance='$paysprovenance' AND cat_id=$tab AND type IN ('p','r','pa','rp')");
					if(count($envoi)==0 AND count($envoi6)==0){
						if($categoriepiece == 'AUTRE'){
							$table1 = array(
								'users_id'=>$id_users,
								'nom'=>$nom,
								'prenom'=>$prenom,
								'nompiece'=>$nompiece,
								'datess'=>$datess,
								'date_piece'=>$dates,
								'cat_id'=>$tab,
								'photo'=>$nomphoto,
								'type'=>'p',
								'pays_provenance'=>$paysprovenance,
								'numero_piece'=>$numeropiece
							);
						}else{
							$table1 = array(
								'users_id'=>$id_users,
								'nom'=>$nom,
								'prenom'=>$prenom,
								'nompiece'=>'',
								'datess'=>$datess,
								'date_piece'=>$dates,
								'cat_id'=>$tab,
								'photo'=>$nomphoto,
								'type'=>'p',
								'pays_provenance'=>$paysprovenance,
								'numero_piece'=>$numeropiece
							);
						}
						$gg = $manager->insertion('pieces',$table1,'');
						$id = $manager->dernierIdInserer();
						echo  json_encode('bien insere');
					}else if(count($envoi)>=1 AND count($envoi6)==0){
						if($id_users == $envoi[0]->users_id){
							echo  json_encode('existant');
						}else{
							$s = array(
								'type'=>'r'
							);
							if($categoriepiece == 'AUTRE'){
								$table1 = array(
									'users_id'=>$id_users,
									'nom'=>$nom,
									'prenom'=>$prenom,
									'nompiece'=>$nompiece,
									'datess'=>$datess,
									'date_piece'=>$dates,
									'cat_id'=>$tab,
									'photo'=>$nomphoto,
									'type'=>'pa',
									'pays_provenance'=>$paysprovenance,
									'numero_piece'=>$numeropiece
								);
							}else{
								$table1 = array(
									'users_id'=>$id_users,
									'nom'=>$nom,
									'prenom'=>$prenom,
									'nompiece'=>'',
									'datess'=>$datess,
									'date_piece'=>$dates,
									'cat_id'=>$tab,
									'photo'=>$nomphoto,
									'type'=>'pa',
									'pays_provenance'=>$paysprovenance,
									'numero_piece'=>$numeropiece
								);
							}
							$manager->insertion('pieces',$table1,'');
							$id = $manager->dernierIdInserer();
							$s = array(
								'type'=>'r'
							);
							$users_id_possesseur = $envoi[0]->users_id;
							$manager->modifier('pieces',$s,"users_id=$users_id_possesseur AND nom like '%$nom%' AND prenom like '%$prenom%' AND datess='$datess' AND pays_provenance='$paysprovenance' AND cat_id=$tab AND type='s'");
							$recupidpiece = $manager->selectionUnique2('pieces',array('*'),"users_id=$users_id_possesseur AND nom like '%$nom%' AND prenom like '%$prenom%' AND datess='$datess' AND pays_provenance='$paysprovenance' AND cat_id=$tab AND type='r'");
							$recupidpiece = $recupidpiece[0]->id_piece;
										$token = $manager->selectionUnique2('users',array('token'),"id_users=$users_id_possesseur");
										$token = $token[0]->token;
										
										$table2 = array(
											'msg_id'=>1,
											'users_id'=>$users_id_possesseur
										);
										$manager->insertion('envoyer',$table2,'');
										
										function send_notification($token,$langue){
											if($langue =='fr'){
												$messagetest = 'Votre pièce a été retrouvée';
											}else{
												$messagetest = 'Your piece has been found';
											}
											
											$url = 'https://fcm.googleapis.com/fcm/send';
											$message = array(
												'title' => 'Find',
												'body' => $messagetest,
												'sound' => 'default'
											);
											$fields = array(
												'to'=>$token,  
												'notification'=>$message
											);
											$headers = array(
													'Authorization:key=AAAA79X7bw4:APA91bHkPqKS69YpSxiVmvndCpI92gl8X_qAuF5C3rq_KttnH9dg95tsCZgBbKRDWmOApMM7xxk-4IYHtPqLBfySlCKImWn7WAZXzrKSN7VR0RtPm5cgIP-yhnPFlx8K1lRo_tPoeIfW',
													'Content-Type:application/json'
											);
											$ch = Curl_init();
											Curl_setopt($ch,CURLOPT_URL,$url);
											Curl_setopt($ch,CURLOPT_POST,true);
											Curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
											Curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
											Curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
											Curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($fields));
											$result = curl_exec($ch);
											if($result === FALSE){
												die('Curl failed: '.curl_error($ch));
											}
											curl_close($ch);
											return $result;
										}
										send_notification($token,$langue);
										echo  json_encode($recupidpiece);
						}			
					}else if(count($envoi6)>=1){
						echo  json_encode('existant');
					}
				}
					
			// }	
		}
	}else{
		echo  json_encode('erreur');
	}
?>