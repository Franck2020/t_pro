<?php
header("Access-Control-Allow-Origin: *");
	if(isset($_POST)){
		include_once('../model/bigModelForMe.php');
		include_once('../model/JWT.php');
		$jwt = new JWT();
		$nbre = $jwt->oauth($_POST['token_oauth']);
		if($nbre == 0){
			$id_users = $_POST['id_users'];
			$id_piece = $_POST['id_piece'];
			$motif = $_POST['motif'];
			function send_notification($token,$corps){
				$url = 'https://fcm.googleapis.com/fcm/send';
				$message = array(
					'title' => 'Find',
					'body' => $corps,
					'sound' => 'default'
				);
				$fields = array(
					'to'=>$token,  
					'notification'=>$message
				);
				$headers = array(
						'Authorization:key=AAAA79X7bw4:APA91bHkPqKS69YpSxiVmvndCpI92gl8X_qAuF5C3rq_KttnH9dg95tsCZgBbKRDWmOApMM7xxk-4IYHtPqLBfySlCKImWn7WAZXzrKSN7VR0RtPm5cgIP-yhnPFlx8K1lRo_tPoeIfW',
						'Content-Type:application/json'
				);
				$ch = Curl_init();
				Curl_setopt($ch,CURLOPT_URL,$url);
				Curl_setopt($ch,CURLOPT_POST,true);
				Curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
				Curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
				Curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
				Curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($fields));

				$result = curl_exec($ch);
				if($result === FALSE){
					die('Curl failed: '.curl_error($ch));
				}
				curl_close($ch);
				return $result;
			}
			if($motif == 'prise'){
				$recup = $manager->selectionUnique2('pieces',array('*'),"id_piece=$id_piece");
				
				$nom = $recup[0]->nom;
				$prenom = $recup[0]->prenom;
				$numero_piece = $recup[0]->numero_piece;
				$datess = $recup[0]->datess;
				$nompiece = $recup[0]->nompiece;
				$pays_provenance = $recup[0]->pays_provenance;
				$cat_id = $recup[0]->cat_id;
				$env1 = $manager->selectionUnique2('pieces',array('*'),"nom like '%$nom%' AND datess='$datess' AND prenom like '%$prenom%' AND pays_provenance='$pays_provenance' AND cat_id=$cat_id AND type='pa'");
				$id_piece2 = $env1[0]->id_piece;
				$id_posteur = $env1[0]->users_id;
				
				$user_posteur = $manager->selectionUnique2('users',array('*'),"id_users=$id_posteur");
				$montant = $user_posteur[0]->remboursement;
				$token = $user_posteur[0]->token;
				$r = array(
					'remboursement'=>intval($montant)+1500
				);
				$r8 = $manager->modifier('users',$r,"id_users=$id_posteur");
				$table2 = array(
					'msg_id'=>14,
					'users_id'=>$id_posteur
				);
				$manager->insertion('envoyer',$table2,'');
				send_notification($token,'1500 FCFA vous sera envoyée dans quelque instant');
				
				$manager->supprimer('pieces',"id_piece=$id_piece");
				$manager->supprimer('pieces',"id_piece=$id_piece2");
				$manager->supprimer('envoyer',"users_id=$id_users AND msg_id=1");
				$manager->supprimer('envoyer',"users_id=$id_users AND msg_id=11");
				echo json_encode($motif);
			}else if($motif == 'nonprise'){
				$recup = $manager->selectionUnique2('pieces',array('*'),"id_piece=$id_piece");
				
				
				$nom = $recup[0]->nom;
				$prenom = $recup[0]->prenom;
				$numero_piece = $recup[0]->numero_piece;
				$datess = $recup[0]->datess;
				$nompiece = $recup[0]->nompiece;
				$pays_provenance = $recup[0]->pays_provenance;
				$cat_id = $recup[0]->cat_id;
				$env1 = $manager->selectionUnique2('pieces',array('*'),"nom like '%$nom%' AND datess='$datess' AND prenom like '%$prenom%' AND pays_provenance='$pays_provenance' AND cat_id=$cat_id AND type='pa'");
				$id_piece2 = $env1[0]->id_piece;
				$id_posteur = $env1[0]->users_id;
				
				$user_possesseur = $manager->selectionUnique2('users',array('*'),"id_users=$id_users");
				$montant = $user_possesseur[0]->remboursement;
				$token = $user_possesseur[0]->token;
				$r = array(
					'remboursement'=>intval($montant)+2500
				);
				$r8 = $manager->modifier('users',$r,"id_users=$id_users");
				$table2 = array(
					'msg_id'=>15,
					'users_id'=>$id_posteur
				);
				$manager->insertion('envoyer',$table2,'');
				send_notification($token,'2500 FCFA vous sera envoyée dans quelque instant');
				
				$r1 = array(
					'type'=>'s'
				);
				$r8 = $manager->modifier('pieces',$r1,"id_users=$id_users");
				$manager->supprimer('pieces',"id_piece=$id_piece2");
				$manager->supprimer('envoyer',"users_id=$id_users AND msg_id=1");
				$manager->supprimer('envoyer',"users_id=$id_users AND msg_id=11");
				
				echo json_encode('non prise');
			}	
		}
			
		
		
		
	}	
	
?>