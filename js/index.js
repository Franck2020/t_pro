$(function(){ 
// localStorage.setItem('monnaie','3000');
		// var url = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
		// fetch(url)
		// .then(res => res.blob())
		// .then(blob => console.log(blob))
		 
		
		$('body').on('click','#resetpage_errorconnection',function(e){
            location.reload();
        });
		$('body').on('click','#masquepage',function(e){
			if(sessionStorage.getItem('parametreouvert') == 'oui'){
				 $('#afficheparametre').attr('style','display:none');
				$('body #masquepage').attr('style','display:none');
				$('body #masquepage').css({
				   'z-index':'4000'
			   });
			   sessionStorage.setItem('parametreouvert','non');
			}
           
        });
		$('body').on('click','#btnparametre',function(e){
			sessionStorage.setItem('parametreouvert','oui');
           $('#afficheparametre').removeAttr('style');
		   $('body #masquepage').removeAttr('style');
		   $('body #masquepage').css({
			   'z-index':'4500'
		   });
        });
		$('body').on('click','.affichermdp',function(e){
			e.preventDefault();
			if($('body .mdp').hasClass('invisibless')){
				var valeur = $('body mdp').val();
				$('body .mdp').attr('type','text');
				$('body .mdp').attr('value',valeur);
				$('body .mdp').removeClass('invisibless');
				$('body .mdp').addClass('visibless');
				$('body .affichermdp span').removeClass('glyphicon-eye-close');
				$('body .affichermdp span').addClass('glyphicon-eye-open');
			}else{
				$('body .mdp').attr('type','password');
				$('body .mdp').addClass('visibless');
				$('body .mdp').addClass('invisibless');
				$('body .affichermdp span').removeClass('glyphicon-eye-open');
				$('body .affichermdp span').addClass('glyphicon-eye-close');
			}
		})
	//*****************************************mes plugins******************************************
		$('body').tagant_verifi_droit();
		$('body').tagant_submit_form('#form_enregistrement');
		$('body').tagant_submit_form('#form_compte');
		$('body').tagant_submit_form('#form_connexion');
		$('body').tagant_submit_form('#form_recherche');
		$('body').tagant_submit_form('#form_message');
		$('body').tagant_submit_form('#form_ajoutmespieces');
		$('body').tagant_affiche_infos('.boutonaccueil','click');
		$('body').tagant_affiche_infos('.boutonselect','change');
		$('body').tagant_affiche_infos('.retour','click');
		$('body').tagant_submit_form_photos2('#form_ajoutpieceperdu');
		$('body').tagant_affiche_messages();
		
	//*************************************************************************************************
		       
	//********************affiche photo formulaire pieceperdu********************
		$('body').on('change','#photopiece',function(e){
			e.preventDefault();
			sessionStorage.setItem('typephotopris','galerie');
			$('#appareil_photo2').attr('src','images/appareil_photo.jpg');
			$('#appareil_photo2').attr('height','15%');
			$('#appareil_photo2').attr('width','15%');
			
			var output_format = '';
			var quality =0;
			var f = e.target.files[0];
			
			var file = this.files[0];
			var fileType = file["type"];
			var fileSize = Math.round(file.size/1024);
			// alert(fileSize+'kb');
			var validImageTypes = ["image/jpeg","image/jpg","image/png"];
			if ($.inArray(fileType,validImageTypes) < 0) {
			
			}else{
				 quality =  80;
				// output file format (jpg || png || webp)
				if(fileType == "image/jpeg" || fileType == "image/jpg"){
					 output_format = 'jpeg'
				}else if(fileType == "image/png"){
					 output_format = 'png'
				}
				//This function returns an Image Object 

				sessionStorage.setItem('videoaulieurimage','false');
				sessionStorage.setItem('quality',quality);
				sessionStorage.setItem('output_format',output_format);
			}
			
            var fileName = f.name;
			sessionStorage.setItem('nomimageajoutpieceperdu',fileName);
            var reader = new FileReader();
			  // Closure to capture the file information.
			reader.addEventListener("load",function(){
				$('#appareil_photo').attr('src',reader.result);
				sessionStorage.setItem('valeurimageajoutpieceperdu',reader.result);
				$('#appareil_photo').attr('height','25%');
				$('#appareil_photo').attr('width','50%');
				// alert(reader.result);
				// preview.src = reader.result;
			},false);
			reader.readAsDataURL(f);
				
        });
		$('body').on('click','#appareil_photo',function(e){
            $('#photopiece').trigger('click');
        });
	//*********************fin affiche photo formulaire pieceperdu********************
	
	//***********************fin des enregistrements apres enregistrement terminé**********************
		$('body').on('click','#enregistrement_termine',function(e){
            sessionStorage.setItem('enregistrement_termine','vrai');
        });
		$('body').on('click','#enregistrement_termine2',function(e){
            sessionStorage.setItem('enregistrement_termine2','vrai');
        });
	//*****************************fin***********************************************************

	//*******************************clique ici si recherche echec*******************************
		$('body').on('click','#enregistrerdansaretrouver',function(e){
			e.preventDefault();
			var pays = sessionStorage.getItem('pays_provenance');
			var categorie = sessionStorage.getItem('categories');
			var numero_piece = sessionStorage.getItem('numero_piece');
			var nom = sessionStorage.getItem('nom');
			var prenom = sessionStorage.getItem('prenom');
			var datess = sessionStorage.getItem('datess');
            $.ajax({
				url:'https://findtagant.online/model/ajoutpiecearetrouve.php',
				type:'post',
				data:'pays='+pays+'&categorie='+categorie+'&numero_piece='+numero_piece+'&id_users='+localStorage.getItem('id_users')+'&nom='+nom+'&prenom='+prenom+'&datess='+datess+'&token_oauth='+localStorage.getItem('token_oauth'),
				beforeSend:function(){
					$('body #masquepage').removeAttr('style');
					$('body #afficheload').removeAttr('style');
					$('body #afficheload').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
				},
				success:function(data){
					if(data != 'existant'){
						$('body #afficheload').html();
						$('body #masquepage').attr('style','display:none');
						$('body #afficheload').attr('style','display:none');
						$.ajax({
							url:'vue/accueil.html',
							type:'post',
							data:'',
							success:function(datas){
								$('#contenu').html(datas);
								$('#alerteur').removeAttr('style');
								$("#alerteur").fadeIn(1000).delay(1500).fadeOut(1000);
							}
						})
					}else{
						if(localStorage.getItem('langue')== 'fr'){
							alert('Cette pièce existe déja');
						}else{
							alert('This piece already exists');
						}
					}
					
				}
			})
        });
	//****************************************************fin************************************

	//*****************************************supprime***********************************
		$('body').on('click','button.supp,button.deletemapiece,button.supparetrouve,button.suppperdu,button.supppayer',function(e){
			e.preventDefault();
			var textte = '';
			if(localStorage.getItem('langue')== 'fr'){
				textte = 'Vous etes sur le point de supprimer cette pièce';
			}else{
				textte = 'You are about to delete this piece';
			}
			if(confirm(textte)){
				var th = $(this);
				// var parentss = th.parent().parent().parent();
				// var valsupp = parentss.attr('name');
				if(th.hasClass('supp')){
					var tof = '';
					var parent1 = th.parent().parent();
					var id_piece = parent1.attr('name');
					
					var parentss = th.parent().parent().parent();
					var valsupp = parentss.attr('name');
					$('body').tagant_delete(id_piece,valsupp,tof);
					parentss.remove();
				}else if(th.hasClass('supppayer')){
					var tof = '';
					var parent1 = th.parent().parent().parent();
					var id_piece = parent1.attr('name');
					
					var parentss = th.parent().parent().parent().parent();
					var valsupp = parentss.attr('name');
					$('body').tagant_delete(id_piece,valsupp,tof);
					parentss.remove();
				}else if(th.hasClass('supparetrouve')){
					var tof = '';
					var parent1 = th.parent().parent();
					var id_piece = parent1.attr('name');
					
					var parentss = th.parent().parent().parent();
					var valsupp = parentss.attr('name');
					$('body').tagant_delete(id_piece,valsupp,'');
					parentss.remove();
				}else if(th.hasClass('suppperdu')){
					var tof = '';
					var parent1 = th.parent().parent();
					var id_piece = parent1.attr('name');
					
					var parentss = th.parent().parent().parent();
					var valsupp = parentss.attr('name');
					if(valsupp == 'pa' || valsupp=='p'){
						tof = parentss.attr('class');
					}
					$('body').tagant_delete(id_piece,valsupp,tof);
					parentss.remove();
				}else if(th.hasClass('deletemapiece')){
					var id_piece = th.parent().parent().attr('name');
					var parentss = th.parent().parent().parent();
					var valsupp = parentss.attr('name');
					$('body').tagant_delete(id_piece,valsupp,'');
					parentss.remove();
				}
				$('body').tagant_affiche_messages();
			}
        });
		
		$('body').on('click','button.confirmerecupiece,button.confirmenonrecupiece',function(e){
			e.preventDefault();
				var th = $(this);
				var id_piece = th.parent().parent().parent().attr('name');
				var parentss = th.parent().parent().parent().parent();
				var textte = '';
				if(th.hasClass('confirmerecupiece')){
					if(localStorage.getItem('langue')== 'fr'){
						textte = 'En cliquant sur ok vous confirmez avoir reçu votre pièce et celle-ci sera completement supprimée';
					}else{
						textte = 'By clicking ok you confirm that you have received your piece and it will be completely deleted';
					}
					if(confirm(textte)){
						$.ajax({
							url:'https://findtagant.online/controleur/perceptionpiece.php',
							type:'post',
							dataType:'json',
							data:'motif=prise&id_users='+localStorage.getItem('id_users')+'&id_piece='+id_piece+'&token_oauth='+localStorage.getItem('token_oauth'),
							success:function(data){
								parentss.remove();
							}
						})
					}
				}else if(th.hasClass('confirmenonrecupiece')){
					if(localStorage.getItem('langue')== 'fr'){
						textte = 'En cliquant sur ok vous confirmez n\'avoir pas reçu votre pièce et 2500 FCFA vous serons remboursée';
					}else{
						textte = 'By clicking on ok you confirm that you have not received your coin and 2500 FCFA will be refunded';
					}
					if(confirm(textte)){
						$.ajax({
							url:'https://findtagant.online/controleur/perceptionpiece.php',
							type:'post',
							dataType:'json',
							data:'motif=nonprise&id_users='+localStorage.getItem('id_users')+'&id_piece='+id_piece+'&token_oauth='+localStorage.getItem('token_oauth'),
							success:function(data){
								parentss.remove();
							}
						})
					}
				}
		});
	//*************************************************fin **********************************************
	
	//********************************affiche mode de paiement**************
		$('body').on('click','.appel',function(e){
			var th = $(this);
			if(localStorage.getItem('monnaie') >= 3000){
				var id_piece = th.attr('name');
				$.ajax({
					url:'https://findtagant.online/model/selectAll.php',
					type:'post',
					dataType:'json',
					data:'motif=id_trouveur&id_users='+localStorage.getItem('id_users')+'&id_piece='+id_piece+'&token_oauth='+localStorage.getItem('token_oauth'),
					success:function(data){
						var mesoptions = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">\
											<center><button style="display:inline-block" class="confirmerecupiece btn btn-success glyphicon glyphicon-thumbs-up"></button></button></center>\
										</div>\
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">\
											<center><button style="display:inline-block" class="confirmenonrecupiece btn btn-warning glyphicon glyphicon-thumbs-down"></button></center>\
										</div>';
						var parenttr = th.parent().parent().parent();
						parenttr.children('td').each(function(d){
							var th2 = $(this);
							var n = th2.attr('class');
							if(n == 'options'){
								th2.html(mesoptions);
								pp =  th2.attr('id');
							}
						})
						th.replaceWith('<a href="tel:+'+data+'"><span>'+data+'</span><br><img src="images/imgtel.jpg" alt="" class="img-responsive img-circle" height="25%" width="25%"></a>');
						var nouveaumontant = localStorage.getItem('monnaie')-3000;
						localStorage.setItem('monnaie',nouveaumontant);
						// th.parent().attr('href',data);
						// th.parent().prepend('<span>'+data+'</span><br>');
					}
				})
			}else{
				var montant = 0;
				var perceveur = '';
				if(localStorage.getItem('pays_id') == 'Cameroon'){
					montant = 3000;
					perceveur = '<img src="images/logomtn_paiement.png" width="15%">  650183735  /  <img width="25%" src="images/logoorange_paiement.jpg">  694559484';
				}else{
					montant = 5000;
					perceveur = '07116876 / 05297504';
				}
				var valeurintg = '<p><center>Pour obtenir le numéro de téléphone de celui possédant votre pièce il vous faudra éffectuer un dépot de '+montant+' au numéro <button class="alert alert-success">'+perceveur+'</button> et votre identifiant avec lequel vous devrez l\'effectuer est le <button class="alert alert-warning">'+localStorage.getItem('tel1')+'</button></center></p>';
				// var pp = '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" id="mode_paiement" style="display:none">\
					// <button type="button" style="background-color:red" id="suppcadrepaiement" class="pull-right">X</button>\
					// <h4 style="color:black"><center>Mode d\'envoi</center></h4>\
					// <div id="mode_paiement_conteneur">'+valeurintg+'</div>\
				// </div>';
				$('#mode_paiement').removeAttr('style');
				$('#mode_paiement_conteneur').html(valeurintg);
			}
	  });
		
		$('body').on('click','#suppcadrepaiement',function(e){
            $(this).parent().attr('style','display:none');
        });
	
	//****************************************
	
	
	$('body').on('change','.choixcategories',function(e){
			e.preventDefault();
			var valeur = $(this).val();
			if(valeur == 'AUTRE'){
				$('body div.nompiece').removeAttr('style');
			}else{
				$('body div.nompiece').attr('style','display:none');
			}
			
    });
	$('body').on('change','#changerpays',function(e){
			e.preventDefault();
			var valeur = $(this).val();
			var textte = '';
			if(localStorage.getItem('langue')== 'fr'){
				textte = 'Attention changer de pays supprimera toutes les pièces sauf vos pièces enregistrées';
			}else{
				textte = 'Attention changing country will delete all parts except your saved parts';
			}
			if(confirm(textte)){
				$.ajax({
					url:'https://findtagant.online/model/changerpays.php',
					type:'post',
					dataType:'json',
					data:'pays='+valeur+'&id_users='+localStorage.getItem('id_users')+'&token_oauth='+localStorage.getItem('token_oauth'),
					beforeSend:function(){
						$('body #masquepage').removeAttr('style');
						$('body #afficheload').removeAttr('style');
						$('body #afficheload').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
					},
					success:function(data){
						$('body #afficheload').html();
						$('body #masquepage').attr('style','display:none');
						$('body #afficheload').attr('style','display:none');
						$('body #affichemesmsg').html();
						localStorage.setItem('pays_id',valeur);
						localStorage.removeItem('drapeau');
						$('body').tagant_affichedrapeau();
						$('body').tagant_affiche_messages();
					}
				})
			}
    });
	$('body').on('click','.recherchemapiece',function(e){
			e.preventDefault();
			var textte = '';
			if(localStorage.getItem('langue')== 'fr'){
				textte = 'Cette pièce ne fera plus partie de vos pièces enregistrées';
			}else{
				textte = 'This piece will no longer be part of your saved pieces';
			}
			if(confirm(textte)){
				var th = $(this).parent().parent().parent();
				var id_users = localStorage.getItem('id_users');
				var nometprenom = '';
				var datess = '';
				var numero_piece = '';
				var pp = '';
				var categories = '';
				var id_piece = '';
				th.children('td').each(function(d){
					var th2 = $(this);
					var n = th2.attr('name');
					var textee = $(this).text();
					if(n == 'nometprenom'){
						nometprenom = textee;
						pp =  th2.attr('id');
					}else if(n == 'datess'){
						datess = textee;
						categories =  th2.attr('id');
						categories = categories.replace('_', " ");
						categories = categories.replace('_', " ");
					}else if(n == 'numero_piece'){
						numero_piece = textee;
					}
				})
				 $.ajax({
					url:'https://findtagant.online/model/cherchemapiece.php',
					type:'post',
					dataType:'json',
					data:'nometprenom='+nometprenom+'&pp='+pp+'&categories='+categories+'&id_users='+localStorage.getItem('id_users')+'&numero_piece='+numero_piece+'&datess='+datess+'&token_oauth='+localStorage.getItem('token_oauth'),
					beforeSend:function(){
						$('body #masquepage').removeAttr('style');
						$('body #afficheload').removeAttr('style');
						$('body #afficheload').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
					},
					success:function(data){
						$('body #afficheload').html();
						$('body #masquepage').attr('style','display:none');
						$('body #afficheload').attr('style','display:none');
						if(data['trouver'] == 'oui'){
												var liste = '';
												var nometprenom = '';
												var nump = '';
												$.ajax({
													url:'vue/success_recherche.html',
													type:'post',
													data:'',
													success:function(data1){
														for(var j in data){
															if(j == 'nom'){
																nom = data[j];
															}else if(j == 'prenom'){
																nometprenom = '<td><center>'+nom+' '+data[j]+'</center></td>'; 
															}else if(j == 'numero_piece'){
																nump = '<td><center>'+data[j]+'</center></td>'; 
															}else if(j == 'id_piece'){
																id_piece = data[j]; 
															}
														}
														 liste = '<tr>'+nometprenom+nump+'<td><center><img src="images/imgtel.jpg" alt="" class="appel img-responsive img-circle" height="25%" width="25%" name="'+id_piece+'"></center></td><td><center><a href="">Voir</a></center></td></tr>';
														$('#contenu').html(data1);
														$('body #corps_reservations').html(liste);
													}
												})
											}else if(data['trouver'] == 'non'){
												for(var p in data){
													if(p != 'trouver'){
														sessionStorage.setItem(p,data[p]);
													}
												}
												$.ajax({
													url:'vue/echec_recherche.html',
													type:'post',
													data:'',
													success:function(datas){
														$('#contenu').html(datas);
													}
												})
											}
						
						
						
						
						}
					})
			}
        });
	$('header #enteteenveloppe').on('click','div#boutonmsg',function(e){
		e.preventDefault();
		var msg = sessionStorage.getItem('msg');
       $('body #modalmessage').modal('show');
	   $('body #affichemesmsg').html(msg);
    });
	$('body').on('click','.voirlapiece',function(e){
		e.preventDefault();
		var th = $(this);
		var nom = th.attr('name');
		nom = nom.replace('rienaymettrejeremplacelevide',' ');
		$('body #modalimagepiece').modal('show');
		$('body #imagepiece').html('<img src="https://findtagant.online/imagesserveur/'+nom+'" class="img-responsive col-xs-12 col-sm-12 col-md-12 col-lg-12" width="50%" height="25%">');

		$.ajax({
			url:'https://findtagant.online/controleur/donneebase64.php',
			type:'post',
			dataType:'json',
			data:'nom='+nom,
			success:function(data){
				$('body #imagepiece').html('<img src="'+data+'" id="monimageaagrandir" class="img-responsive col-xs-12 col-sm-12 col-md-12 col-lg-12" width="100%" height="100%">');
				// $('body').tagant_attachPinch('body div#imagepiece','body img.monimageaagrandir');
				$('body').tagant_hammerIt(document.getElementById("monimageaagrandir"));
			}
		})
    });
	
	
	$('body').on('click','.parametre',function(e){
				   e.preventDefault();
				   var th = $(this);
				   var nom = th.attr('name');
				   var nom_users = localStorage.getItem('nom');
				   if(nom == 'compte' && nom_users != undefined){
					   $('body #modalcompte').modal('show');
					   $('#afficheparametre').attr('style','display:none');
					   var str = localStorage.getItem('datess')
					   var datess = str.split('-');
					   $('body #form_compte input').each(function(){
							var th2 = $(this);
							var nom2 = th2.attr('name');
							if(nom2 =='nom'){
								th2.val(localStorage.getItem('nom'));
							}else if(nom2 =='prenom'){
								th2.val(localStorage.getItem('prenom'));
							}else if(nom2 =='jour'){
								th2.val(datess[0]);
							}else if(nom2 =='mois'){
								th2.val(datess[1]);
							}else if(nom2 =='annee'){
								th2.val(datess[2]);
							}else if(nom2 =='tel1'){
								th2.val(localStorage.getItem('tel1'));
							}else if(nom2 =='email'){
								th2.val(localStorage.getItem('email'));
							}
					   });
				   }else if(nom == 'deconnexion' && nom_users != null){
					   $('#afficheparametre').attr('style','display:none');
					   $.ajax({
							url:'vue/optionaccueil.html',
							type:'post',
							data:'',
							success:function(data){
								$('body #contenu').html(data); 
								page = $('#contenu div').attr('name');
								$('body').tagant_changelangue(localStorage.getItem('langue'),page);
								var langue = localStorage.getItem('langue');
								localStorage.clear();
								localStorage.setItem('langue',langue);
							}
						})
				   }else if(nom == 'desinscription' && nom_users != null){
					   $('#afficheparametre').attr('style','display:none');
					   if(localStorage.getItem('langue')== 'fr'){
							 cordova.plugins.email.open({
								to: localStorage.getItem('email'),
								subject: 'Désinscription',
								body:    'Je veux me désinscrire'
							});
						}else{
							 cordova.plugins.email.open({
								to: localStorage.getItem('email'),
								subject: 'Unsubscribe',
								body:    'I want to unsubscribe'
							});
						}
				   }else if(nom_users == null){
						if(localStorage.getItem('langue')== 'fr'){
							alert('Veuillez vous inscrire pour y avoir accès');
						}else{
							alert('Please register to get access');
						}
				   }
				});
	
	
})

 