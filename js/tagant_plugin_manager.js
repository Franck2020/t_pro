 (function($){
	 sessionStorage.setItem('enregistrement_termine','faux');
	 sessionStorage.setItem('enregistrement_termine2','faux');
	 // localStorage.clear();
	 $('body div#entetedrapeau #imagedudrapeau').on('load', function(){
			$('body #masquepage2').attr('style','display:none');
			$('body #afficheload').attr('style','display:none');
		});
	 if(localStorage.getItem('pays_id') == null){
		  localStorage.setItem('pays_id','drapeau_blanc');
	 }
	//*************************change de langue *******************************************************
		
		function mestab(donnee){
			var afficheparametre = {
				'fr':['Compte','Déconnexion','Désinscription'],
				'en':['account','Sign Out','Unsubscribe']
			};
			var accueil = {
				'fr':['Rechercher une pièce','Ajouter les pièces perdues','Enregistrer mes pièces','Liste de vos pièces:','A trouver','Trouvée','Changer de pays','Vos suggestions'],
				'en':['Find a piece','Add lost pieces','Save my pieces','List of your pieces:','To find','found','Change your country','Your suggestions']
			};
			var error_internet = {
				'fr':['HORS CONNEXION','Vérifier que votre connexion passe bien ensuite réessayer'],
				'en':['OFFLINE','Check that your connection is ok then try again']
			};
			var piecearetrouve = {
				'fr':['Liste des pièces à retrouver','(Cette liste comporte les pièces que vous souhaitez retrouver et dont vous serez notifié si tel est le cas.)','retour','Nom et prénom','Date','Numéro','Options'],
				'en':['List of pieces to find','(This list contains the pieces you want to find and you will be notified if this is the case.)',' return','Name and first name','Date','Number','Options']
			};
			var rechercher = {
				'fr':['retour','Rechercher votre pièce','Nom:','Prénom:','Date de naissance:','Pays de provenance de la pièce:','Catégorie de la pièce:','Numéro de la pièce:','rechercher'],
				'en':[' return','Find your piece','Name:','First name:','Birth of date:','Country of origin of the piece:','Category of the piece:','Number of the piece:','Find']
			};
			var success_recherche = {
				'fr':['Pièce trouvée','(Cette liste comporte vos numéros de pièces et de ceux  qui les detiennent abrégé ici par téléphone_det.)','retour','Nom et prénom','Numéro','Téléphone_det','Photo'],
				'en':['Piece found','(This list includes your part numbers and those who hold them abbreviated here by phone_det.)',' return','Name and first name','Number:','Phone_det','Photo']
			};
			var suggestions = {
				'fr':['Envoyez nous vos suggestions','retour','(Nous vous prions de ne nous envoyer que des avis constructifs qui permettront ou contribueront à l\'amelioration de notre mode de vie.)','Envoyer'],
				'en':['Send us your suggestions',' return','(Please only send us constructive advice that will help or contribute to the improvement of our way of life.)','To send']
			};
			var ajoutmespieces = {
				'fr':['retour','Ajouter mes pièces','(cliquer sur enregistré après chaque enregistrement et sur enregistrement terminer après avoir terminé d\'enregistrer)','Nom:','Prénom','Date de naissance (jj/mm/aaaa)','Pays de provenance de la pièce:','Catégorie de la pièce:','Numéro de la pièce:','Enregistré','Enregistrements terminer'],
				'en':[' return','Add my pieces','(click on recorded after each recording and on recording to finish after having finished recording)','Name:','First name','Birth of date (dd/mm/yyyy)','Country of origin of the piece:','Category of the piece:','Number of the piece:','Save','Recordings finish']
			};
			var ajoutpieceperdu = {
				'fr':['retour','Ajout pièce(s) perdue(s)','(cliquer sur enregistré après chaque enregistrement et sur enregistrement terminer après avoir terminé d\'enregistrer)','Nom:','Prénom:','Date de naissance (jj/mm/aaaa):','Pays de provenance de la pièce:','Catégorie de la pièce:','Numéro de la pièce:','Photo de la pièce:','Enregistrer','Enregistrements terminer'],
				'en':[' return','Adding lost piece(s)','(click on recorded after each recording and on recording to finish after having finished recording)','Name:','First name:','Birth of date (dd/mm/yyyy):','Country of origin of the piece:','Category of the piece:','Number of the piece:','Photo of the piece:','Save','Recordings finish']
			};
			var echec_rechercher = {
				'fr':['Aucun resultat trouvé pour le moment','retour','Pour ajouter votre recherche à la liste des pièces à retrouvées pour que en cas de publication vous soyez notifié,','cliquer ici'],
				'en':['No results found for the moment',' return','To add your search to the list of pieces to be found so that in case of publication you are notified,','click here']
			};
			var modalcompte = {
				'fr':['Informations personnelles','Nom:','Prénom:','Date de naissance (jj/mm/aaaa)','Téléphone','Nouveau mot de passe','Email','Modifier'],
				'en':['Personal informations','Name:','First name:','Birth of date (dd/mm/yyyy)','Phone','New Password:','Email',' Edit']
			};
			var enregistrement = {
				'fr':['Enregistrement','(Les noms suivis d\'un asterix sont obligatoires)','Dans quel pays vous trouvez-vous?','Nom:','Prénom:','Date de naissance (jj/mm/aaaa)','Téléphone','Mot de passe','sexe','Email','Je reconnais avoir lu et compris les',' conditions générales d\'utilisation',' et je les accepte','Valider'],
				'en':['Recording','(Names followed by an asterix are mandatory)','In which country do you find yourself?','Name:','First name:','Birth of date (dd/mm/yyyy)','Phone','Password','sex','Email','I acknowledge having read and understood the',' Terms of Service',' and I accept them',' Validate']
			};
			var connexion = {
				'fr':['Connexion','','Téléphone','Mot de passe','Valider'],
				'en':['Connection','',' Phone',' Password','Validate']
			};
			var listemespieces = {
				'fr':['Mes pièces enregistrées','retour','Ajout','Choix','Unique','Multiple','Nom et prénom','Date','Numéro','Type','Options'],
				'en':['My saved pieces',' return','Adding','Choice','Unique','Multiple','Name and first name','Date','Number','Type','Options']
			};
			var listepieceperdu = {
				'fr':['Liste des pièces perdues','(Les pièces dans les champs en vert sont ceux retrouvés par leurs possesseurs)','retour','Ajout','Choix','Unique','Multiple','Nom et prénom','Date','Numéro','Options'],
				'en':['List of lost pieces(s)','(The pieces in the green fields are those found by their owners)',' return','Adding','Choice','Unique','Multiple','Name and first name','Date','Number','Options']
			};
			var listepieceretrouve = {
				'fr':['Liste des pièces retrouvées','(Cette liste comporte vos de pièces retrouvées et le numéro de ceux qui les detiennent abrégé ici par téléphone_det.)','retour','Nom et prénom','Numéro','Téléphone_det','Photo','Options'],
				'en':['List of found pieces','(This list contains your pieces found and the number of those who hold them abbreviated here by phone_det.)',' return','Name and first name','Number','phone_det','Photo','Options']
			};
			if(donnee =='accueil'){
				return accueil;
			}else if(donnee =='piecearetrouve'){
				return piecearetrouve;
			}else if(donnee =='error_internet'){
				return error_internet;
			}else if(donnee =='rechercher'){
				return rechercher;
			}else if(donnee =='success_recherche'){
				return success_recherche;
			}else if(donnee =='suggestions'){
				return suggestions;
			}else if(donnee =='ajoutmespieces'){
				return ajoutmespieces;
			}else if(donnee =='ajoutpieceperdu'){
				return ajoutpieceperdu;
			}else if(donnee =='echec_rechercher'){
				return echec_rechercher;
			}else if(donnee =='enregistrement'){
				return enregistrement;
			}else if(donnee =='listemespieces'){
				return listemespieces;
			}
			else if(donnee =='listepieceretrouve'){
				return listepieceretrouve;
			}else if(donnee =='listepieceperdu'){
				return listepieceperdu;
			}else if(donnee =='connexion'){
				return connexion;
			}else if(donnee =='afficheparametre'){
				return afficheparametre;
			}else if(donnee =='modalcompte'){
				return modalcompte;
			}
		}
		$('body').on('change','select#languess',function(e){
			e.preventDefault();
            var th = $(this);
			var langue = th.val();
			var page = $('#contenu div').attr('name');
			localStorage.setItem('langue',langue);
			sessionStorage.setItem('page',page);
			var ppp = $('body #error_connect').text();
			if(langue =='fr'){
				if(page == 'suggestions'){
					$('body .textareaen').replaceWith('<textarea class="form-control z-depth-1 textareafr" name="message" style="color:#008080" id="exampleFormControlTextarea6" rows="3" placeholder="Ecrivez ici..."></textarea>');
				}else if(page == 'optionaccueil'){
					$('body .enregistreren').replaceWith('<input type="button" class="btn btn-primary form-control boutonaccueil  input enregistrerfr" name="enregistrement" value="S\'enregistrer">');
					$('body .connexionen').replaceWith('<input type="button" class="btn btn-success form-control boutonaccueil  input connexionfr" name="connexion" value="Se connecter">');
				}
				$('body #contenu .langue').each(function(d){
					var th2 = $(this);
					if(th2.hasClass('input')){
						th2.val(mestab(page).fr[d]);
					}else{
						if(th2.text()=='Name:'){
							var parentss = th2.parent().parent();
							th2.text(mestab(page).fr[d]);
							parentss.children('body .inputen').replaceWith('<input type="text" class="form-control inputfr" name="nom" placeholder="Nom" maxlength="10" minlength="2" required>');
						}else if(th2.text()=='First name:'){
							var parentss = th2.parent().parent();
							th2.text(mestab(page).fr[d]);
							parentss.children('body .inputen').replaceWith('<input type="text" class="form-control inputfr" name="prenom" placeholder="Prenom" maxlength="10" minlength="2" required>');
						}else{
							th2.text(mestab(page).fr[d]);
							if(page == 'connexion'){
								if(ppp !=''){
									$('body #error_connect').text('Veuillez vérifier vos données entrées');
								}
							}
						}
					}
				})
				$('body div#afficheparametre .langue').each(function(d){
					var page2 = 'afficheparametre';
					var th2 = $(this);
					th2.text(mestab(page2).fr[d]);
				})
				$('body div#modalcompte .langue').each(function(d){
					var page2 = 'modalcompte';
					var th2 = $(this);
					th2.text(mestab(page2).fr[d]);
				})
			}else{
				if(page == 'suggestions'){
					$('body .textareafr').replaceWith('<textarea class="form-control z-depth-1 textareaen" name="message" style="color:#008080" id="exampleFormControlTextarea6" rows="3" placeholder="Write here..."></textarea>');
				}else if(page == 'optionaccueil'){
					$('body .enregistrerfr').replaceWith('<input type="button" class="btn btn-primary form-control boutonaccueil  input enregistreren" name="enregistrement" value="register">');
					$('body .connexionfr').replaceWith('<input type="button" class="btn btn-success form-control boutonaccueil  input connexionen" name="connexion" value="to log in">');
				}
				$('body #contenu .langue').each(function(d){
					var th2 = $(this);
					if(th2.hasClass('input')){
						th2.val(mestab(page).en[d]);
					}else{
						if(th2.text()=='Nom:'){
							var parentss = th2.parent().parent();
							th2.text(mestab(page).en[d]);
							parentss.children('body .inputfr').replaceWith('<input type="text" class="form-control inputen" name="nom" placeholder="Name" maxlength="10" minlength="2" required>');
						}else if(th2.text()=='Prénom:'){
							var parentss = th2.parent().parent();
							th2.text(mestab(page).en[d]);
							parentss.children('body .inputfr').replaceWith('<input type="text" class="form-control inputen" name="prenom" placeholder="First name" maxlength="10" minlength="2" required>');
						}else{
							th2.text(mestab(page).en[d]);
							if(page == 'connexion'){
								if(ppp !=''){
									$('body #error_connect').text('Please check your entered data');
								}
							}
						}
					}
				})
				$('body div#afficheparametre .langue').each(function(d){
					var page2 = 'afficheparametre';
					var th2 = $(this);
					th2.text(mestab(page2).en[d]);
				})
				$('body div#modalcompte .langue').each(function(d){
					var page2 = 'modalcompte';
					var th2 = $(this);
					th2.text(mestab(page2).en[d]);
				})
			}
        });
		jQuery.fn.tagant_changelangue = function(langue,page){
			var ppp = $('body #error_connect').text();
			if(langue =='fr'){
				if(page == 'suggestions'){
					$('body .textareaen').replaceWith('<textarea class="form-control z-depth-1 textareafr" name="message" style="color:#008080" id="exampleFormControlTextarea6" rows="3" placeholder="Ecrivez ici..."></textarea>');
				}else if(page == 'optionaccueil'){
					$('body .enregistreren').replaceWith('<input type="button" class="btn btn-primary form-control boutonaccueil  input enregistrerfr" name="enregistrement" value="S\'enregistrer">');
					$('body .connexionen').replaceWith('<input type="button" class="btn btn-success form-control boutonaccueil  input connexionfr" name="connexion" value="Se connecter">');
				}
				$('body #contenu .langue').each(function(d){
					var th2 = $(this);
					if(th2.hasClass('input')){
						th2.val(mestab(page).fr[d]);
					}else{
						if(th2.text()=='Name:'){
							var parentss = th2.parent().parent();
							th2.text(mestab(page).fr[d]);
							parentss.children('body .inputen').replaceWith('<input type="text" class="form-control inputfr" name="nom" placeholder="Nom" maxlength="10" minlength="2" required>');
						}else if(th2.text()=='First name:'){
							var parentss = th2.parent().parent();
							th2.text(mestab(page).fr[d]);
							parentss.children('body .inputen').replaceWith('<input type="text" class="form-control inputfr" name="prenom" placeholder="Prenom" maxlength="10" minlength="2" required>');
						}else{
							th2.text(mestab(page).fr[d]);
							if(page == 'connexion'){
								if(ppp !=''){
									$('body #error_connect').text('Veuillez vérifier vos données entrées');
								}
							}
						}
					}
				})
				$('body div#afficheparametre .langue').each(function(d){
					var page2 = 'afficheparametre';
					var th2 = $(this);
					th2.text(mestab(page2).fr[d]);
				})
				$('body div#modalcompte .langue').each(function(d){
					var page2 = 'modalcompte';
					var th2 = $(this);
					th2.text(mestab(page2).fr[d]);
				})
			}else{
				if(page == 'suggestions'){
					$('body .textareafr').replaceWith('<textarea class="form-control z-depth-1 textareaen" name="message" style="color:#008080" id="exampleFormControlTextarea6" rows="3" placeholder="Write here..."></textarea>');
				}else if(page == 'optionaccueil'){
					$('body .enregistrerfr').replaceWith('<input type="button" class="btn btn-primary form-control boutonaccueil  input enregistreren" name="enregistrement" value="register">');
					$('body .connexionfr').replaceWith('<input type="button" class="btn btn-success form-control boutonaccueil  input connexionen" name="connexion" value="to log in">');
				}
				$('body #contenu .langue').each(function(d){
					var th2 = $(this);
					if(th2.hasClass('input')){
						th2.val(mestab(page).en[d]);
					}else{
						if(th2.text()=='Nom:'){
							var parentss = th2.parent().parent();
							th2.text(mestab(page).en[d]);
							parentss.children('body .inputfr').replaceWith('<input type="text" class="form-control inputen" name="nom" placeholder="Name" maxlength="10" minlength="2" required>');
						}else if(th2.text()=='Prénom:'){
							var parentss = th2.parent().parent();
							th2.text(mestab(page).en[d]);
							parentss.children('body .inputfr').replaceWith('<input type="text" class="form-control inputen" name="prenom" placeholder="First name" maxlength="10" minlength="2" required>');
						}else{
							th2.text(mestab(page).en[d]);
							if(page == 'connexion'){
								if(ppp !=''){
									$('body #error_connect').text('Please check your entered data');
								}
							}
						}
					}
				})
				$('body div#afficheparametre .langue').each(function(d){
					var page2 = 'afficheparametre';
					var th2 = $(this);
					th2.text(mestab(page2).en[d]);
				})
				$('body div#modalcompte .langue').each(function(d){
					var page2 = 'modalcompte';
					var th2 = $(this);
					th2.text(mestab(page2).en[d]);
				})
			}
			if(page == 'rechercher' || page == 'ajoutmespieces' || page == 'ajoutpieceperdu' || page == 'enregistrement'){
				$('body').tagant_affichepays();
			}
			
			if(page == 'ajoutmespieces' || page == 'rechercher'){
				$('body').tagant_affiche_nom_prenom();
			}
		}
		
	 //***********************************fin change langue*******************************
	 //*********************************recup pays **********************************
				//******************transform image et affiche***********************
								
								jQuery.fn.tagant_affichedrapeau = function(){
									var urlt ='';
									$('body #masquepage2').removeAttr('style');
									$('body #afficheload').removeAttr('style');
									$('body #afficheload').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
									if(localStorage.getItem('drapeau') == null){
										function toDataURL(url, callback) {
											var httpRequest = new XMLHttpRequest();
											httpRequest.onload = function() {
											   var fileReader = new FileReader();
												  fileReader.onloadend = function() {
													 callback(fileReader.result);
												  }
												  fileReader.readAsDataURL(httpRequest.response);
											};
											httpRequest.open('GET', url);
											httpRequest.responseType = 'blob';
											httpRequest.send();
										 }
										 if(localStorage.getItem('pays_id') == 'drapeau_blanc'){
											urlt =  'drapeaus_pays/';
										 }else{
											urlt = 'https://findtagant.online/drapeaus_pays/';
										 }
										 toDataURL(urlt+localStorage.getItem('pays_id')+'.jpg', function(dataUrl){
											 localStorage.setItem('drapeau',dataUrl);
											 $('body div#entetedrapeau #imagedudrapeau').attr('src',dataUrl);
									  })
									}else{
										$('body div#entetedrapeau #imagedudrapeau').attr('src',localStorage.getItem('drapeau'));
									}
								}
								// then call this from anywhere
								
							 //***********************************fin*******************************
	 //*************************************fin recup position*************************************
	 	
					//**********mes plugins*************************
	 
	 //************traite soumission form **********************************************************
					jQuery.fn.tagant_submit_form = function(form_soumis){
						this.on('submit',form_soumis,function(e){
							e.preventDefault();
							var dismoisichecked = true;
							var quelcase = '';
							var th= $(this);
							var url = th.data('url');
							var partss = th.serialize();
							if(form_soumis == '#form_message'){
								var partss = th.serialize()+'&id_users='+localStorage.getItem('id_users')+'&token_oauth='+localStorage.getItem('token_oauth');
							}else if(form_soumis == '#form_recherche'){
								var partss = th.serialize()+'&id_users='+localStorage.getItem('id_users')+'&token_oauth='+localStorage.getItem('token_oauth');
							}else if(form_soumis == '#form_ajoutmespieces'){
								var partss = th.serialize()+'&id_users='+localStorage.getItem('id_users')+'&token_oauth='+localStorage.getItem('token_oauth');
							}else if(form_soumis == '#form_compte'){
								var partss = th.serialize()+'&id_users='+localStorage.getItem('id_users')+'&token_oauth='+localStorage.getItem('token_oauth');
							}else if(form_soumis == '#form_enregistrement'){
								if($('body input[name=sexe]:checked').val() == undefined){
									dismoisichecked = false;
								}else if($('body input#case').prop('checked') == false){
									dismoisichecked = false;
									quelcase = 'cgu';
								}
							}
							if(dismoisichecked){
								$.ajax({
									url:url,
									type:'post',
									dataType:'json',
									data:partss,
									beforeSend:function(){
										$('body #masquepage').removeAttr('style');
										$('body #afficheload').removeAttr('style');
										$('body #afficheload').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
									},
									success:function(data){
										$('body #afficheload').html();
										$('body #masquepage').attr('style','display:none');
										$('body #afficheload').attr('style','display:none');
										if(form_soumis =='#form_ajoutmespieces'){
											if(data != 'existant'){
												 if($('body #enregistrement_termine2').attr('style') == 'display:none' || sessionStorage.getItem('enregistrement_termine2') == 'vrai'){
														$('body #reseteur').trigger('click');
														$.ajax({
															url:'vue/accueil.html',
															type:'post',
															data:'',
															success:function(datas){
																$('#contenu').html(datas);
																sessionStorage.setItem('enregistrement_termine2','faux');
																var page = $('#contenu div').attr('name');
																$('body').tagant_changelangue(localStorage.getItem('langue'),page);
															}
													})
												}else if($('body #enregistrement_termine2').attr('style') != 'display:none' || sessionStorage.getItem('enregistrement_termine2') == 'faux'){
														$('body #reseteur').trigger('click');
														$('body #masquepage').attr('style','display:none');
														$('body #afficheload').attr('style','display:none');
												}
											}else{
												if(localStorage.getItem('langue')== 'fr'){
													alert('Existe déja');
												}else{
													alert('Already exists');
												}
											}
										}else if(form_soumis =='#form_enregistrement'){
											if(data != 'existant'){
												localStorage.setItem('token_oauth',data[1]);
												if(data[0][0].nom != '' && typeof(data[0][0].nom) != 'undefined'){
													localStorage.removeItem('pays_id');
													localStorage.removeItem('drapeau');
													for(var i in data[0][0]){
															localStorage.setItem(i,data[0][0][i]);
														}
														$('body').tagant_affichedrapeau();
														$.ajax({
															url:'vue/accueil.html',
															type:'post',
															data:'',
															success:function(datas){
																$('#contenu').html(datas);
																var page = $('#contenu div').attr('name');
																$('body').tagant_changelangue(localStorage.getItem('langue'),page);
																// $('#alerteur').removeAttr('style');
																// $("#alerteur").fadeIn(1000).delay(1500).fadeOut(1000);
															}
														})
														$('body').tagant_affiche_messages();
														 $('body').tagant_affichenotification();
																$.ajax({
																	url:'https://findtagant.online/controleur/saveToken.php',
																	type:'post',
																	dataType:'json',
																	data:'token='+localStorage.getItem('token')+'&id_users='+localStorage.getItem('id_users')+'&token_oauth='+localStorage.getItem('token_oauth'),
																	success:function(datass){}
																})
														// location.reload();
												}else{
													if(localStorage.getItem('langue')== 'fr'){
														alert("Verifier vos donneés entreés");
													}else{
														alert("Check your data in between");
													}
												}
											}else{
												if(localStorage.getItem('langue')== 'fr'){
													alert('Existe déja');
												}else{
													alert('Already exists');
												}
											}	
										}else if(form_soumis =='#form_connexion'){
											if(data != ''){
												localStorage.setItem('token_oauth',data[1]);
												localStorage.removeItem('pays_id');
												localStorage.removeItem('drapeau');
												for(var i in data[0][0]){
														localStorage.setItem(i,data[0][0][i]);
													}
													
												$('body').tagant_verifi_droit();
													
													$('body').tagant_affichedrapeau();
													$.ajax({
														url:'vue/accueil.html',
														type:'post',
														data:'',
														success:function(datas){
															$('#contenu').html(datas);
															var page = $('#contenu div').attr('name');
															$('body').tagant_changelangue(localStorage.getItem('langue'),page);
															// $('#alerteur').removeAttr('style');
															// $("#alerteur").fadeIn(1000).delay(1500).fadeOut(1000);
														}
													})
													$('body').tagant_affiche_messages();
													$('body').tagant_affichenotification();
															$.ajax({
																url:'https://findtagant.online/controleur/saveToken.php',
																type:'post',
																dataType:'json',
																data:'token='+localStorage.getItem('token')+'&id_users='+localStorage.getItem('id_users')+'&token_oauth='+localStorage.getItem('token_oauth'),
																success:function(datass){}
															})
											}else{
												if(localStorage.getItem('langue')== 'fr'){
													$('body #error_connect').text('Veuillez vérifier vos données entrées');
												}else{
													$('body #error_connect').text('Please check your entered data');
												}
												
											}
										}else if(form_soumis == '#form_recherche'){
											if(data['trouver'] == 'oui'){
												var liste = '';
												var nom = '';
												var prenom = '';
												var nometprenom = '';
												var nump = '';
												var id_piece = '';
												var photo = '';
												$.ajax({
													url:'vue/success_recherche.html',
													type:'post',
													data:'',
													success:function(data1){
														for(var j in data[0]){
															if(j == 'nom'){
																 nom = data[0][j];
															}else if(j == 'prenom'){
																nometprenom = '<td><center>'+nom+' '+data[0][j]+'</center></td>'; 
															}else if(j == 'numero_piece'){
																nump = '<td><center>'+data[0][j]+'</center></td>'; 
															}else if(j == 'id_piece'){
																id_piece =data[0][j]; 
															}else if(j == 'photo'){
																photo =data[0][j]; 
																photo = photo.replace(' ', "rienaymettrejeremplacelevide");
															}
														}
														 liste = '<tr>'+nometprenom+nump+'<td><center><img src="images/imgtel.jpg" alt="" class="appel img-responsive img-circle" height="25%" width="25%" name="'+id_piece+'"></center></td><td><center><a href="" name='+photo+' class="voirlapiece">Voir</a></center></td></tr>';
														$('#contenu').html(data1);
														$('body #corps_reservations').html(liste);
													}
												})
											}else if(data['trouver'] == 'non'){
												for(var p in data){
													if(p != 'trouver'){
														sessionStorage.setItem(p,data[p]);
													}
												}
												$.ajax({
													url:'vue/echec_recherche.html',
													type:'post',
													data:'',
													success:function(datas){
														$('#contenu').html(datas);
													}
												})
											}
										}else if(form_soumis == '#form_message'){
											$.ajax({
												url:'vue/accueil.html',
												type:'post',
												data:'',
												success:function(data){
													$('#contenu').html(data);
													$('#alerteur').removeAttr('style');
													$("#alerteur").fadeIn(1000).delay(1500).fadeOut(1000);
													var page = $('#contenu div').attr('name');
													$('body').tagant_changelangue(localStorage.getItem('langue'),page);
												}
											})
										}else if(form_soumis == '#form_compte'){
											if(data == ''){
												if(localStorage.getItem('langue')== 'fr'){
													alert('Ces numéros de téléphones sont déja attribués veuillez les changer');
												}else{
													alert('These phone numbers are already assigned please change them');
												}
												
											}else{
												for(var i in data[0]){
													localStorage.setItem(i,data[0][i]);
												}
												$('body button.close').trigger('click');
												$('body #reseteurformcompte').trigger('click');
											}
										}
									}
								})
							}else if(quelcase == 'cgu'){
								if(localStorage.getItem('langue')== 'fr'){
									alert('Veuillez cocher la case liée à nos conditions d\'utilisation');
								}else{
									alert('Please check the box related to our terms of use');
								}
							}else if(quelcase == ''){
								if(localStorage.getItem('langue')== 'fr'){
									alert('Veuillez choisir votre sexe');
								}else{
									alert('Please choose your gender');
								}
							}
						})
					}
			//**************fin traitement **********************************************************
			
			 //************traite recuperation donneés bd*************************************************
					jQuery.fn.tagant_recup = function(donnee){
						var liste ='';
						var dates ='';
						var nom= '';
						var nometprenom ='';
						var id_piece ='';
						var listetrouvee ='';
						var type ='';
						var tel ='';
						var photo ='';
						var type ='';
						var pp ='';
						var categories ='';
						var tel2 ='237693159279';
							$.ajax({
								url:'https://findtagant.online/model/selectAll.php',
								type:'post',
								dataType:'json',
								data:'motif='+donnee+'&id_users='+localStorage.getItem('id_users')+'&token_oauth='+localStorage.getItem('token_oauth'),
								beforeSend:function(){
									if(donnee =='pieceperdu'){
										$('body #corps_reservations').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
										$('body #corps_reservations').attr('style','background-color:white');
									}else if(donnee =='mespievces'){
										$('body #corps_reservations4').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
										$('body #corps_reservations4').attr('style','background-color:white');
									}else if(donnee =='piecearetrouves'){
										$('body #corps_reservations2').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
										$('body #corps_reservations2').attr('style','background-color:white');
									}else if(donnee =='listepieceretrouve'){
										$('body #corps_reservations3').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
										$('body #corps_reservations3').attr('style','background-color:white');
									}
								},
								success:function(data){
									if(donnee =='pieceperdu'){
										for(var i in data){
											for(var j in data[i]){
												if(j == 'nom'){
													 nom = data[i][j];
												}else if(j == 'prenom'){
													nometprenom = '<td><center>'+nom+' '+data[i][j]+'</center></td>'; 
												}else if(j == 'datess'){
													dates = '<td><center>'+data[i][j]+'</center></td>'; 
												}else if(j == 'numero_piece'){
													nump = '<td><center>'+data[i][j]+'</center></td>'; 
												}else if(j == 'id_piece'){
													id_piece = data[i][j]; 
												}else if(j == 'photo'){
													photo = data[i][j]; 
												}else if(j == 'type' && data[i][j]=='pa'){
													type = 'pa';
												}
											}
											if(type == 'pa'){
												listetrouvee += '<tr name="pa" class="'+photo+'" style="background-color:#00FF00">'+nometprenom+dates+nump+'<td class="options" name="'+id_piece+'"><center><button class="suppperdu btn btn-danger glyphicon glyphicon-trash"></button></center></td>'+'</tr>';
											}else{
												liste += '<tr name="p" class="'+photo+'">'+nometprenom+dates+nump+'<td class="option" name="'+id_piece+'"><center><button class="suppperdu btn btn-danger glyphicon glyphicon-trash"></button></center></td>'+'</tr>';
											}
										}
										liste = listetrouvee+liste;
										$('body #corps_reservations').html(liste);
									}else if(donnee =='mespievces'){
										for(var i in data){
											var compte = 0;
											var compte2 = 0;
											for(var j in data[i]){
												if(compte ==0){
													pp = data[i]['pays_provenance'];
													compte++;
												}else if(compte2 ==0){
													categories = data[i]['cat_id'];
													categories = categories.replace(' ', "_");
													categories = categories.replace(' ', "_");
													compte2++;
												}
												
												
												if(j == 'nom'){
													nom = data[i][j];
												}else if(j == 'prenom'){
													nometprenom = '<td name="nometprenom" id='+pp+'><center>'+nom+' '+data[i][j]+'</center></td>'; 
												}else if(j == 'datess'){
													dates = '<td name="datess" id='+categories+'><center>'+data[i][j]+'</center></td>'; 
												}else if(j == 'numero_piece'){
													nump = '<td name="numero_piece"><center>'+data[i][j]+'</center></td>'; 
												}else if(j == 'id_piece'){
													id_piece = data[i][j]; 
												}else if(j == 'cat_id'){
													type ='<td><center>'+data[i][j]+'</center></td>';
												}
											}
											liste += '<tr name="simplesupp">'+nometprenom+dates+nump+type+'<td class="col-xs-12 col-sm-12 col-md-12 col-lg-12" name="'+id_piece+'"><center><button class="col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xs-offset-1 col-xs-5 col-sm-5 col-md-5 col-lg-5 btn btn-danger glyphicon glyphicon-trash deletemapiece"></button><button class="col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xs-offset-1 col-xs-5 col-sm-5 col-md-5 col-lg-5 btn btn-primary glyphicon glyphicon-search recherchemapiece"></button></center></td>'+'</tr>';
										}
										$('body #corps_reservations4').html(liste);
									}else if(donnee =='piecearetrouves'){
										for(var i in data){
											for(var j in data[i]){
												if(j == 'nom'){
													nom = data[i][j];
												}else if(j == 'prenom'){
													nometprenom = '<td><center>'+nom+' '+data[i][j]+'</center></td>'; 
												}else if(j == 'datess'){
													dates = '<td><center>'+data[i][j]+'</center></td>'; 
												}else if(j == 'numero_piece'){
													nump = '<td><center>'+data[i][j]+'</center></td>'; 
												}else if(j == 'id_piece'){
													id_piece = data[i][j]; 
												}
											}
											liste += '<tr name="simplesupp">'+nometprenom+dates+nump+'<td class="options" name="'+id_piece+'"><center><button class="supparetrouve btn btn-danger glyphicon glyphicon-trash"></button></center></td>'+'</tr>';
										}
										$('body #corps_reservations2').html(liste);
									}else if(donnee =='listepieceretrouve'){
										for(var i in data){
											for(var j in data[i][0]){
												if(j == 'nom'){
													nom = data[i][0][j];
												}else if(j == 'prenom'){
													nometprenom = '<td><center>'+nom+' '+data[i][0][j]+'</center></td>'; 
												}else if(j == 'numero_piece'){
													nump = '<td><center>'+data[i][0][j]+'</center></td>'; 
												}else if(j == 'id_piece'){
													id_piece = data[i][0][j]; 
												}else if(j == 'tel'){
													tel = '<span>'+data[i][0][j]+'</span><br>';
													tel2 = data[i][0][j];
												}else if(j == 'photo'){
													photo = data[i][0][j];
													photo = photo.replace(' ', "rienaymettrejeremplacelevide");
												}
											}
											if(tel ==''){
												liste += '<tr name="trouvee" class="'+photo+'">'+nometprenom+nump+'<td><center><img src="images/imgtel.jpg" alt="" class="appel img-responsive img-circle" height="25%" width="25%" name="'+id_piece+'"></center></td><td><center><a href="" name='+photo+' class="voirlapiece">Voir</a></center></td><td class="options" name="'+id_piece+'"><center><button class="supp btn btn-danger glyphicon glyphicon-trash"></button></center></td><tr>';

											}else{
												var mesoptions = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">\
																		<center><button style="display:inline-block" class="confirmerecupiece btn btn-success glyphicon glyphicon-thumbs-up"></button></button></center>\
																	</div>\
																	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">\
																		<center><button style="display:inline-block" class="confirmenonrecupiece btn btn-warning glyphicon glyphicon-thumbs-down"></button></center>\
																	</div>';
												liste += '<tr name="trouvee" class="'+photo+'">'+nometprenom+nump+'<td><center><a href="tel:+'+tel2+'">'+tel+'<img src="images/imgtel.jpg" alt="" class="img-responsive img-circle" height="25%" width="25%" name="'+id_piece+'"></a></center></td><td><center><a href="" name='+photo+' class="voirlapiece">Voir</a></center></td><td class="options" name="'+id_piece+'">'+mesoptions+'</td><tr>';
											}
											tel ='';
											tel2 ='237693159279';
										}
										$('body #corps_reservations3').html(liste);
									}
								}
							})
						}
			//**************fin traitement recuperation*****************************************************
			
			
			
			//affiche form_inscription,a_propos,form_connexion******************
				jQuery.fn.tagant_affiche_infos = function(bouton,evenementss){
					this.on(evenementss,bouton,function(e){
						e.preventDefault();
						var th= $(this)
						var nom = th.attr('name');
						if(nom == 'enregistrement' || nom == 'connexion'){
							$('body #optionsaccesaccueil').attr('style','display:none');
						}
						var nom2 = '';
						var url = '';
						if(nom == 'select'){
							 nom2= th.val();
							if(nom2 != 'Liste de vos pièces:'){
								 url = 'vue/'+nom2+'.html';
							}
						}else if(nom == 'select2'){
							 nom2= th.val();
							 url = 'vue/ajoutpieceperdu.html';
						}else if(nom == 'select3'){
							 nom2= th.val();
							 url = 'vue/ajoutmespieces.html';
						}
						else{
							 url = 'vue/'+nom+'.html';
						}
						if(nom2 != 'Liste de vos pièces:' && nom2 != 'List of your pieces:' && nom2 != 'Choix'){
							if(nom2 == 'piecearetrouves'){
								$.ajax({
									url:url,
									type:'post',
									data:'',
									success:function(data){
										$('#contenu').html(data);
										var page = $('#contenu div').attr('name');
										$('body').tagant_changelangue(localStorage.getItem('langue'),page);
										$('body').tagant_recup(nom2);
									}
								})
							}else if(nom2 == 'listepieceretrouve'){
								$.ajax({
									url:url,
									type:'post',
									data:'',
									success:function(data){
										$('#contenu').html(data);
										var page = $('#contenu div').attr('name');
										$('body').tagant_changelangue(localStorage.getItem('langue'),page);
										$('body').tagant_recup(nom2);
									}
								})
							}else{
								$.ajax({
									url:url,
									type:'post',
									data:'',
									success:function(data){
										$('#contenu').html(data);
										var page = $('#contenu div').attr('name');
										$('body').tagant_changelangue(localStorage.getItem('langue'),page);
										if(nom2 == 'unique'){
											$('body #enregistrement_termine2').attr('style','display:none');
											$('body #enregistrement_termine').attr('style','display:none');
										}else if(nom == 'listepieceperdu'){
											$('body').tagant_recup('pieceperdu');
										}else if(nom == 'listemespieces'){
											$('body').tagant_recup('mespievces');
										}
									}
								})
							}
						}
					})
				}
	 
			//******affiche les villes******************
						jQuery.fn.tagant_affiche_messages = function(){
							var msg = '';
							var compte = 0;
							$.ajax({
											url:'https://findtagant.online/model/affiche_les_messages.php',
											type:'post',
											dataType:'json',
											data:'id_users='+localStorage.getItem('id_users')+'&token_oauth='+localStorage.getItem('token_oauth'),
											success:function(data){
												for(var p in data){
													if(p == 'montant'){
														localStorage.setItem('monnaie',parseInt(data[p]));
													}
													for(var k in data[p]){
														if(localStorage.getItem('langue')== 'fr'){
															if(k == 'msg_id' && data[p][k] == 1){
																msg += '<div style="text-align:center" class="form-group">\
																		<h4 class="alert alert-success">Pièce trouvée</h4>\
																		<p><center>Votre pièce à été retrouvée rendez-vous dans la liste des pièces souhaitées pour la voir</center></p>\
																		<hr>\
																	</div>';
															}else if(k == 'msg_id' && data[p][k] == 11){
																msg += '<div style="text-align:center" class="form-group">\
																		<h4 class="alert alert-success">Montant bien reçu</h4> \
																		<p><center>Vous pouvez maintenant voir le numéro de celui qui possède votre pièce en recliquant sur le bouton correspondant à Telephone_det</center></p>\
																		<hr>\
																	</div>';
															}if(k == 'msg_id' && data[p][k] == 14){
																msg += '<div style="text-align:center" class="form-group">\
																		<h4 class="alert alert-info">Informations</h4>\
																		<p><center>1500 FCFA vous sera envoyée dans quelque instant</center></p>\
																		<hr>\
																	</div>';
															}else if(k == 'msg_id' && data[p][k] == 15){
																msg += '<div style="text-align:center" class="form-group">\
																		<h4 class="alert alert-warning">Informations</h4> \
																		<p><center>2500 FCFA vous sera envoyée dans quelque instant</center></p>\
																		<hr>\
																	</div>';
															}
														}else{
															if(k == 'msg_id' && data[p][k] == 1){
																msg += '<div style="text-align:center" class="form-group">\
																		<h4 class="alert alert-success">Piece found</h4>\
																		<p><center>Your piece has been found go to the list of desired parts to see it</center></p>\
																		<hr>\
																	</div>';
															}else if(k == 'msg_id' && data[p][k] == 11){
																msg += '<div style="text-align:center" class="form-group">\
																		<h4 class="alert alert-success">Amount received</h4> \
																		<p><center>You can now see the number of the person who owns your room by clicking on the button corresponding to Telephone_det</center></p>\
																		<hr>\
																	</div>';
															}if(k == 'msg_id' && data[p][k] == 14){
																msg += '<div style="text-align:center" class="form-group">\
																		<h4 class="alert alert-info">Informations</h4>\
																		<p><center>1500 FCFA will be sent to you in a few moments</center></p>\
																		<hr>\
																	</div>';
															}else if(k == 'msg_id' && data[p][k] == 15){
																msg += '<div style="text-align:center" class="form-group">\
																		<h4 class="alert alert-warning">Informations</h4> \
																		<p><center>2500 FCFA will be sent to you in a moment</center></p>\
																		<hr>\
																	</div>';
															}
														}
														
														
													}
													compte++;
												}
												sessionStorage.setItem('msg',msg);
												var textval = $('body #nbremsg').text();
												if(data != 'rien'){
													if(textval == ''){
													$('body #nbremsg').text(compte-1);
													}else{
														var vi = parseInt(textval);
														// $('body #nbremsg').text(vi +1);
														$('body #nbremsg').text(compte-1);
													}
												}
											}
										})
										$('header #enteteenveloppe').on('click','div#boutonmsg',function(e){
											e.preventDefault();
											var msg = sessionStorage.getItem('msg');
										   $('body #modalmessage').modal('show');
										   $('body #affichemesmsg').html(msg);
										});
					}
				
				//***********traite form_*********	
				
				jQuery.fn.tagant_submit_form_photos2 = function(form_soumis2){
					this.on('submit',form_soumis2,function(e){
						e.preventDefault();
						if(sessionStorage.getItem('videoaulieurimage') != 'true' && sessionStorage.getItem('videoaulieurimage') != null){
							if(sessionStorage.getItem('typephotopris') != 'camera'){
								var th= $(this);
								var url = th.data('url');
								var ff = $('form')[0];
								var formas = new FormData(ff);
								var idusers = localStorage.getItem('id_users');
								formas.append("id_users",idusers);
								formas.append("langue",localStorage.getItem('langue'));
								formas.append("token_oauth",localStorage.getItem('token_oauth'));
								if(sessionStorage.getItem('nomimageajoutpieceperdu') != null){
									formas.delete('pp');
									if(sessionStorage.getItem('typephotopris') == 'galerie'){
										var source_img = sessionStorage.getItem('valeurimageajoutpieceperdu');
										var output_format = sessionStorage.getItem('output_format');
										formas.append("extension_fichier",output_format);
										formas.append("pp",source_img);
										formas.append("nomphoto",sessionStorage.getItem('nomimageajoutpieceperdu'));
										
									}
									else if(sessionStorage.getItem('typephotopris') == 'camera'){
										var output_format = sessionStorage.getItem('output_format');
										 var myImage = document.getElementById('appareil_photo2');
										var myCanvas = document.createElement('canvas');
										var ctx = myCanvas.getContext('2d');
											ctx.drawImage(myImage, 0, 0); 
										var myDataURL = myCanvas.toDataURL('image/jpeg');
										
										formas.append("extension_fichier",output_format);
										formas.append("pp",myDataURL);
										formas.append("nomphoto",sessionStorage.getItem('nomimageajoutpieceperdu'));
										
										// var quality = localStorage.getItem('quality');
										// var output_format = localStorage.getItem('output_format');
										// var source_img = document.getElementById('appareil_photo2');
										// var t = jic.compress(source_img,quality,output_format).src;
										// var urlf = t;
										// fetch(urlf)
										// .then(res => res.blob())
										// .then(blob => sessionStorage.setItem('valeurimageajoutpieceperdu',blob))
										// var myFile = new File([sessionStorage.getItem('valeurimageajoutpieceperdu')],sessionStorage.getItem('nomimageajoutpieceperdu'));
										// formas.append("pp",myFile);
									}
											
												
												
								}
										$.ajax({
											url:url,
											data:formas,
											processData:false,
											contentType:false,
											type:'post',
											dataType:'json',
											beforeSend:function(){
												$('body #masquepage').removeAttr('style');
												$('body #afficheload').removeAttr('style');
												$('body #afficheload').html('<center><img src="images/loader.gif" height="30%" width="30%"></center>');
											},
											success:function(data){
												$('body #masquepage').attr('style','display:none');
												$('body #afficheload').attr('style','display:none');
												if(data != 'existant' && data != 'erreur'){
													if(data != 'bien insere'){
														localStorage.setItem(data,'oui');
													}
													if($('body #enregistrement_termine').attr('style') == 'display:none' || sessionStorage.getItem('enregistrement_termine') == 'vrai'){
														$('body #reseteur').trigger('click');
														
														$('body #appareil_photo').attr('src','images/galerie.jpg');
														$('#appareil_photo').attr('height','15%');
														$('#appareil_photo').attr('width','15%');
														
														$('body #appareil_photo2').attr('src','images/appareil_photo.jpg');
														$('#appareil_photo2').attr('height','15%');
														$('#appareil_photo2').attr('width','15%');
														
														$.ajax({
															url:'vue/accueil.html',
															type:'post',
															data:'',
															success:function(datas){
																$('#contenu').html(datas);
																sessionStorage.setItem('enregistrement_termine','faux');
																$('body #masquepage').attr('style','display:none');
																$('body #afficheload').attr('style','display:none');
																var page = $('#contenu div').attr('name');
																$('body').tagant_changelangue(localStorage.getItem('langue'),page);
															}
														})
													}else if($('body #enregistrement_termine').attr('style') != 'display:none' || sessionStorage.getItem('enregistrement_termine') == 'faux'){
														$('body #reseteur').trigger('click');
														
														$('body #appareil_photo').attr('src','images/galerie.jpg');
														$('#appareil_photo').attr('height','15%');
														$('#appareil_photo').attr('width','15%');
														
														$('body #appareil_photo2').attr('src','images/appareil_photo.jpg');
														$('#appareil_photo2').attr('height','15%');
														$('#appareil_photo2').attr('width','15%');
														$('body #masquepage').attr('style','display:none');
														$('body #afficheload').attr('style','display:none');
													}
												}else if(data == 'existant'){
													if(localStorage.getItem('langue')== 'fr'){
														alert('Existe déja');
														$.ajax({
															url:'vue/accueil.html',
															type:'post',
															data:'',
															success:function(dataa){
																$('#contenu').html(dataa);
																$('body #masquepage').attr('style','display:none');
																$('body #afficheload').attr('style','display:none');
																var page = $('#contenu div').attr('name');
																$('body').tagant_changelangue(localStorage.getItem('langue'),page);
															}
														})
													}else{
														alert('Already exists');
														$.ajax({
															url:'vue/accueil.html',
															type:'post',
															data:'',
															success:function(dataa){
																$('#contenu').html(dataa);
																var page = $('#contenu div').attr('name');
																$('body').tagant_changelangue(localStorage.getItem('langue'),page);
															}
														})
													}
												}
											}
										})
							}else{
								if(localStorage.getItem('langue')== 'fr'){
									alert('Veuillez s\'il vous plait choisir l\'autre option c\'est à dire celle de selectionner la photo de la pièce dans votre téléphone');
								}else{
									alert('Please choose the other option, that is to select the photo of the room in your phone');
								}
							}
						}else{
							if(localStorage.getItem('langue')== 'fr'){
								alert('Vous devez envoyer l\'image de la pièce');
							}else{
								alert('You must send the image of the part');
							}
						}
					})
				}
				//************************************************
				jQuery.fn.tagant_delete = function(donnee,valsupp,nomfichier){
						var notification = '';
						if(localStorage.getItem(donnee) != null){
							notification = localStorage.getItem(donnee);
						}
						$.ajax({
							url:'https://findtagant.online/model/delete.php',
							data:'valeur='+donnee+'&id_users='+localStorage.getItem('id_users')+'&notification='+notification+'&valsupp='+valsupp+'&nomfichier='+nomfichier+'&token_oauth='+localStorage.getItem('token_oauth'),
							type:'post',
							dataType:'json',
							success:function(data){
								 if(data == 'change pas'){
									alert('Cette pièce ne peut plus etre modifiée ou supprimée car elle a été retrouvée par son possesseur');
								}
								// if(data == 'pa'){
									// var textval = $('body #nbremsg').text();
									// var vi = parseInt(textval);
									// if(vi >= 1){
										// $('body #nbremsg').text(vi-1);
									// }else{
										// $('body #nbremsg').text('');
									// }
								// }
								
							}
						})
					}
				//************************************************
				jQuery.fn.tagant_hammerIt = function(elm){
						if(elm != null){
							// clearInterval(exec);
							 hammertime = new Hammer(elm, {});
							 hammertime.get('pinch').set({ enable: true }); 
							 var posX = 0, posY = 0, scale = 1, last_scale = 1, last_posX = 0, last_posY = 0, max_pos_x = 0, max_pos_y = 0, transform = "", el = elm;
							 hammertime.on('doubletap pan pinch panend pinchend', function(ev) {
								 if (ev.type == "doubletap") { 
									transform = "translate3d(0, 0, 0) " + "scale3d(2, 2, 1) "; 
									scale = 2; 
									last_scale = 2;
									try {
										if (window.getComputedStyle(el, null).getPropertyValue('-webkit-transform').toString() != "matrix(1, 0, 0, 1, 0, 0)") { transform = "translate3d(0, 0, 0) " + "scale3d(1, 1, 1) ";
											scale = 1; 
											last_scale = 1;
										} 
									} catch (err) {}
									el.style.webkitTransform = transform; transform = ""; 
								}
								if (scale != 1) {
									posX = last_posX + ev.deltaX; posY = last_posY + ev.deltaY; max_pos_x = Math.ceil((scale - 1) * el.clientWidth / 2);
									max_pos_y = Math.ceil((scale - 1) * el.clientHeight / 2); 
									if (posX > max_pos_x) { posX = max_pos_x; }
									if (posX < -max_pos_x) { posX = -max_pos_x; } 
									if (posY > max_pos_y) { posY = max_pos_y; }
									if (posY < -max_pos_y) { posY = -max_pos_y; }
									}
								if (ev.type == "pinch") { scale = Math.max(.999, Math.min(last_scale * (ev.scale), 4));}
								if(ev.type == "pinchend"){last_scale = scale;} 
								if(ev.type == "panend"){ 
									last_posX = posX < max_pos_x ? posX : max_pos_x; last_posY = posY < max_pos_y ? posY : max_pos_y; 
								}
								if (scale != 1) { transform = "translate3d(" + posX + "px," + posY + "px, 0) " + "scale3d(" + scale + ", " + scale + ", 1)"; }						
								if (transform) { el.style.webkitTransform = transform; } 
							});
						}
					// setInterval(exec,2000);
				} 

				jQuery.fn.tagant_attachPinch = function(wrapperID,imgID){
					var image = $(imgID);
					var wrap = $(wrapperID);

					var  width = image.width();
					var  height = image.height();
					var  newX = 0;
					var  newY = 0;
					var  offset = wrap.offset();

					$(imgID).hammer().on("pinch", function(event) {
						var photo = $(this);

						newWidth = photo.width() * event.gesture.scale;
						newHeight = photo.height() * event.gesture.scale;

						// Convert from screen to image coordinates
						var x;
						var y;
						x -= offset.left + newX;
						y -= offset.top + newY;

						newX += -x * (newWidth - width) / newWidth;
						newY += -y * (newHeight - height) / newHeight;

						photo.css('-webkit-transform', "scale3d("+event.gesture.scale+", "+event.gesture.scale+", 1)");      
						wrap.css('-webkit-transform', "translate3d("+newX+"px, "+newY+"px, 0)");

						width = newWidth;
						height = newHeight;
					});

				}
			//***********************fin des plugins*************************
	jQuery.fn.tagant_affichepays = function(){	
		var liste = '';
		 var countries = [
          ["Afghanistan","Af"],
          ["Åland Islands","Ax"],
          ["Albania","Al"],
          ["Algeria","Dz"],
          ["American Samoa","As"],
          ["Andorra","Ad"],
          ["Angola","Ao"],
          ["Anguilla","Ai"],
          ["Antarctica","Aq"],
          ["Antigua and Barbuda","Ag"],
          ["Argentina","Ar"],
          ["Armenia","Am"],
          ["Aruba","Aw"],
          ["Australia","Au"],
          ["Austria","at"],
          ["Azerbaijan","Az"],
          ["Bahamas","Bs"],
          ["Bahrain","Bh"],
          ["Bangladesh","Bd"],
          ["Barbados","Bb"],
          ["Belarus","by"],
          ["Belgium","Be"],
          ["Belize","Bz"],
          ["Benin","Bj"],
          ["Bermuda","Bm"],
          ["Bhutan","Bt"],
          ["Bolivia, Plurinational State of","Bo"],
          ["Bonaire, Saint Eustatius and Saba","Bq"],
          ["Bosnia and Herzegovina","Ba"],
          ["Botswana","Bw"],
          ["Bouvet Island","Bv"],
          ["Brazil","Br"],
          ["British Indian Ocean Territory","Io"],
          ["Brunei Darussalam","Bn"],
          ["Bulgaria","Bg"],
          ["Burkina Faso","Bf"],
          ["Burundi","Bi"],
          ["Cambodia","Kh"],
          ["Cameroon","Cm"],
          ["Canada","Ca"],
          ["Cape Verde","Cv"],
          ["Cayman Islands","Ky"],
          ["Central African Republic","Cf"],
          ["Chad","Td"],
          ["Chile","Cl"],
          ["China","Cn"],
          ["Christmas Island","Cx"],
          ["Cocos (Keeling) Islands","Cc"],
          ["Colombia","Co"],
          ["Comoros","Km"],
          ["Congo","Cg"],
          ["Congo, the Democratic Republic of the","Cd"],
          ["Cook Islands","Ck"],
          ["Costa Rica","Cr"],
          ["Côte D'ivoire","Ci"],
          ["Croatia","Hr"],
          ["Cuba","Cu"],
          ["Curaçao","Cw"],
          ["Cyprus","Cy"],
          ["Czech Republic","Cz"],
          ["Denmark","Dk"],
          ["Djibouti","Dj"],
          ["Dominica","Dm"],
          ["Dominican Republic","Do"],
          ["Ecuador","Ec"],
          ["Egypt","Eg"],
          ["El Salvador","Sv"],
          ["Equatorial Guinea","Gq"],
          ["Eritrea","Er"],
          ["Estonia","Ee"],
          ["Ethiopia","Et"],
          ["Falkland Islands (Malvinas)","Fk"],
          ["Faroe Islands","Fo"],
          ["Fiji","Fj"],
          ["Finland","Fi"],
          ["France","Fr"],
          ["French Guiana","Gf"],
          ["French Polynesia","Pf"],
          ["French Southern Territories","Tf"],
          ["Gabon","Ga"],
          ["Gambia","Gm"],
          ["Georgia","Ge"],
          ["Germany","De"],
          ["Ghana","Gh"],
          ["Gibraltar","Gi"],
          ["Greece","Gr"],
          ["Greenland","Gl"],
          ["Grenada","Gd"],
          ["Guadeloupe","Gp"],
          ["Guam","Gu"],
          ["Guatemala","Gt"],
          ["Guernsey","Gg"],
          ["Guinea","Gn"],
          ["Guinea-Bissau","Gw"],
          ["Guyana","Gy"],
          ["Haiti","Ht"],
          ["Heard Island and Mcdonald Islands","Hm"],
          ["Holy See (Vatican City State)","Va"],
          ["Honduras","Hn"],
          ["Hong Kong","Hk"],
          ["Hungary","Hu"],
          ["Iceland","Is"],
          ["India","in"],
          ["Indonesia","Id"],
          ["Iran, Islamic Republic of","Ir"],
          ["Iraq","Iq"],
          ["Ireland","Ie"],
          ["Isle of Man","Im"],
          ["Israel","Il"],
          ["Italy","It"],
          ["Jamaica","Jm"],
          ["Japan","Jp"],
          ["Jersey","Je"],
          ["Jordan","Jo"],
          ["Kazakhstan","Kz"],
          ["Kenya","Ke"],
          ["Kiribati","Ki"],
          ["Korea, Democratic People's Republic of","Kp"],
          ["Korea, Republic of","Kr"],
          ["Kuwait","Kw"],
          ["Kyrgyzstan","Kg"],
          ["Lao People's Democratic Republic","La"],
          ["Latvia","Lv"],
          ["Lebanon","Lb"],
          ["Lesotho","Ls"],
          ["Liberia","Lr"],
          ["Libyan Arab Jamahiriya","Ly"],
          ["Liechtenstein","Li"],
          ["Lithuania","Lt"],
          ["Luxembourg","Lu"],
          ["Macao","Mo"],
          ["Macedonia, the Former Yugoslav Republic of","Mk"],
          ["Madagascar","Mg"],
          ["Malawi","Mw"],
          ["Malaysia","My"],
          ["Maldives","Mv"],
          ["Mali","Ml"],
          ["Malta","Mt"],
          ["Marshall Islands","Mh"],
          ["Martinique","Mq"],
          ["Mauritania","Mr"],
          ["Mauritius","Mu"],
          ["Mayotte","Yt"],
          ["Mexico","Mx"],
          ["Micronesia, Federated States of","Fm"],
          ["Moldova, Republic of","Md"],
          ["Monaco","Mc"],
          ["Mongolia","Mn"],
          ["Montenegro","Me"],
          ["Montserrat","Ms"],
          ["Morocco","Ma"],
          ["Mozambique","Mz"],
          ["Myanmar","Mm"],
          ["Namibia","Na"],
          ["Nauru","Nr"],
          ["Nepal","Np"],
          ["Netherlands","Nl"],
          ["New Caledonia","Nc"],
          ["New Zealand","Nz"],
          ["Nicaragua","Ni"],
          ["Niger","Ne"],
          ["Nigeria","Ng"],
          ["Niue","Nu"],
          ["Norfolk Island","Nf"],
          ["Northern Mariana Islands","Mp"],
          ["Norway","No"],
          ["Oman","Om"],
          ["Pakistan","Pk"],
          ["Palau","Pw"],
          ["Palestinian Territory, Occupied","Ps"],
          ["Panama","Pa"],
          ["Papua New Guinea","Pg"],
          ["Paraguay","Py"],
          ["Peru","Pe"],
          ["Philippines","Ph"],
          ["Pitcairn","Pn"],
          ["Poland","Pl"],
          ["Portugal","Pt"],
          ["Puerto Rico","Pr"],
          ["Qatar","Qa"],
          ["Réunion","Re"],
          ["Romania","Ro"],
          ["Russian Federation","Ru"],
          ["Rwanda","Rw"],
          ["Saint Barthélemy","Bl"],
          ["Saint Helena, Ascension and Tristan Da Cunha","Sh"],
          ["Saint Kitts and Nevis","Kn"],
          ["Saint Lucia","Lc"],
          ["Saint Martin (French Part)","Mf"],
          ["Saint Pierre and Miquelon","Pm"],
          ["Saint Vincent and the Grenadines","Vc"],
          ["Samoa","Ws"],
          ["San Marino","Sm"],
          ["Sao Tome and Principe","St"],
          ["Saudi Arabia","Sa"],
          ["Senegal","Sn"],
          ["Serbia","Rs"],
          ["Seychelles","Sc"],
          ["Sierra Leone","Sl"],
          ["Singapore","Sg"],
          ["Sint Maarten (Dutch Part)","Sx"],
          ["Slovakia","Sk"],
          ["Slovenia","Si"],
          ["Solomon Islands","Sb"],
          ["Somalia","So"],
          ["South Africa","Za"],
          ["South Georgia and the South Sandwich Islands","Gs"],
          ["Spain","Es"],
          ["Sri Lanka","Lk"],
          ["Sudan","Sd"],
          ["Suriname","Sr"],
          ["Svalbard and Jan Mayen","Sj"],
          ["Swaziland","Sz"],
          ["Sweden","Se"],
          ["Switzerland","Ch"],
          ["Syrian Arab Republic","Sy"],
          ["Taiwan, Province of China","Tw"],
          ["Tajikistan","Tj"],
          ["Tanzania, United Republic of","Tz"],
          ["Thailand","Th"],
          ["Timor-Leste","Tl"],
          ["Togo","Tg"],
          ["Tokelau","Tk"],
          ["Tonga","to"],
          ["Trinidad and Tobago","Tt"],
          ["Tunisia","Tn"],
          ["Turkey","Tr"],
          ["Turkmenistan","Tm"],
          ["Turks and Caicos Islands","Tc"],
          ["Tuvalu","Tv"],
          ["Uganda","Ug"],
          ["Ukraine","Ua"],
          ["United Arab Emirates","Ae"],
          ["United Kingdom","Gb"],
          ["United States","Us"],
          ["United States Minor Outlying Islands","Um"],
          ["Uruguay","Uy"],
          ["Uzbekistan","Uz"],
          ["Vanuatu","Vu"],
          ["Venezuela, Bolivarian Republic of","Ve"],
          ["Viet Nam","Vn"],
          ["Virgin Islands, British","Vg"],
          ["Virgin Islands, U.S.","Vi"],
          ["Wallis and Futuna","Wf"],
          ["Western Sahara","Eh"],
          ["Yemen","Ye"],
          ["Zambia","Zm"],
          ["Zimbabwe","Zw"]
		];
		for(var i in countries){
			if(localStorage.getItem('pays_id') == 'drapeau_blanc' && countries[i][0] == 'Cameroon'){
				liste += '<option selected="selected" value="'+countries[i][0]+'">'+countries[i][0]+'</option>';
			}else if(countries[i][0] == localStorage.getItem('pays_id')){
				liste += '<option selected="selected" value="'+countries[i][0]+'">'+countries[i][0]+'</option>';
			}else{
				liste += '<option value="'+countries[i][0]+'">'+countries[i][0]+'</option>';
			}
			 // $.ajax({
				// url:'model/insertpays.php',
				// data:'countries='+countries[i][0],
				// type:'post',
				// dataType:'json',
				// success:function(data){
				// }
			// })
		}
		 $('body .listepays').html(liste);
	}
	jQuery.fn.tagant_affichenotification = function(){
		var message = ['Votre piece a été retrouvée veuillez verifier la liste des pièce retrouvée','Vous pouvez maintenant voir le numéro de celui qui possède votre pièce en recliquant sur le bouton correspondant à Telephone_det','1500 FCFA vous sera envoyée dans quelque instant','2500 FCFA vous sera envoyée dans quelque instant'];
		var message1 = ['Your piece has been found please check the list of found parts','You can now see the number of the person who owns your room by clicking on the button corresponding to Telephone_det','1500 FCFA will be sent to you in a few moments','2500 FCFA will be sent to you in a moment'];
		
		function trySomeTimes(asyncFunc, onSuccess, onFailure, customTries) {
		  var tries = typeof customTries === "undefined" ? 100 : customTries;
		  var interval = setTimeout(function () {
			if (typeof asyncFunc !== "function") {
			  onSuccess("Unavailable");
			  return;
			}
			asyncFunc()
			  .then(function (result) {
				if ((result !== null && result !== "") || tries < 0) {
				  onSuccess(result);
				} else {
				  trySomeTimes(asyncFunc, onSuccess, onFailure, tries - 1);
				}
			  })
			  .catch(function (e) {
				clearInterval(interval);
				onFailure(e);
			  });
		  }, 100);
		}

		function logFCMToken() {
		  trySomeTimes(
			FCM.getToken,
			function (token) {
				if(localStorage.getItem('token') != null){
					localStorage.setItem('token',token);
				}
				if(localStorage.getItem('id_users') !=null){
					$.ajax({
						url:'https://findtagant.online/controleur/saveToken.php',
						type:'post',
						dataType:'json',
						data:'token='+localStorage.getItem('token')+'&id_users='+localStorage.getItem('id_users'),
						success:function(data){}
					})
				}
			},
			function (error) {
				alert('Error recup token');
			}
		  );
		}
		
		 // setTimeout(getTheToken,2000);
		 
		function setupOnNotification() {
		  FCM.eventTarget.addEventListener(
			"notification",
			function (data) {
				if(data.wasTapped){
					var t = $('body').tagant_affiche_messages();
					setTimeout(t,1000);
				}else{
					var t = $('body').tagant_affiche_messages();
					setTimeout(t,1000);
						
				}
				  
			},
			false
		  );
		  FCM.getInitialPushPayload()
			.then((payload) => {
			  addToLog(
				"<p>Initial Payload</p><pre>" +
				  JSON.stringify(payload, null, 2) +
				  "</pre>"
			  );
			})
			.catch((error) => {
			  addToLog(
				"<p>Initial Payload Error</p><pre>" +
				  JSON.stringify(error, null, 2) +
				  "</pre>"
			  );
			});
		} 
		
		function waitForPermission(callback) {
		  FCM.requestPushPermission()
			.then(function (didIt) {
			  if (didIt) {
				callback();
			  } else {
				alert("Push permission was not given to this application");
			  }
			})
			.catch(function (error) {
			  alert("Error on checking permission: " + error);
			});
		}

		function logHasPermissionOnStart() {
		  FCM.hasPermission().then(function (hasIt) {
			addToLog("<p>Started with permission: " + JSON.stringify(hasIt) + "</p>");
		  });
		}
		
		function setupListeners() {
		  logHasPermissionOnStart();
		  waitForPermission(function () {
			FCM.createNotificationChannel({
			  id: "sound_alert6",
			  name: "Sound Alert6",
			  // description: "Useless",
			  importance: "high",
			  // visibility: "public",
			  sound: "elet_mp3",
			  // lights: false,
			  // vibration: false,
			});
			logFCMToken();
			// logAPNSToken();
			// setupOnTokenRefresh();
			setupOnNotification();
			// setupClearAllNotificationsButton();
		  });
		}
		
		setupListeners();
	}
	
	jQuery.fn.tagant_affiche_nom_prenom = function(){
		$('input[name=nom]').val(localStorage.getItem('nom'));
		$('input[name=prenom]').val(localStorage.getItem('prenom'));
	}
	
	jQuery.fn.tagant_verifi_droit = function(){
		if(localStorage.getItem('role_num') == '1'){
			var role = localStorage.getItem('role_num');
			var nom = localStorage.getItem('nom');
			// alert('Bienvenu administrateur '+nom);
		}
	}
			
	
})(jQuery)