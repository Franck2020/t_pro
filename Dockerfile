FROM beevelop/android-nodejs

ENV CORDOVA_VERSION 10.0.0

WORKDIR "/app"

RUN npm i -g --unsafe-perm cordova@${CORDOVA_VERSION} && \
    cordova --version && \
    cd /app && \
    cordova create Find fr.grafikart.find Find && \
    cd Find && \
    cordova plugin add cordova-plugin-camera --save && \
    cordova plugin add cordova.plugins.diagnostic --save && \
    cordova plugin add cordova-plugin-androidx --save && \
    cordova plugin add cordova-plugin-email --save && \
    cordova plugin add cordova-plugin-geolocation --save && \
    cordova plugin add cordova-plugin-globalization --save && \
    cordova plugin add cordova-plugin-network-information --save && \
    cordova plugin add cordova-plugin-request-location-accuracy --save && \
    cordova plugin add cordova-support-android-plugin --save && \
    cordova plugin add cordova-plugin-fcm-with-dependecy-updated --save && \
    cordova platform add android --save

COPY bootstrap \
     drapeaus_pays \
     images \
     js \
     vue \
     index.html \
     /app/Find/plaforms/android/app/src/main/assets/www/